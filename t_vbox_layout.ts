import * as tests from "./tests";
import * as color from "./core/color";
import {Panel} from "./core/panel";
import * as renderer from "./core/renderer";
import * as evm from "./core/event_manager";
import * as event from "./core/event";
import * as button from "./widgets/button";
import * as radio from "./widgets/radio";
import * as checkbox from "./widgets/checkbox";
import * as vbox from "./layout/vbox";

export function run() {

    let canvas = document.createElement("canvas");
    canvas.width = 800;
    canvas.height = 800;
    let body = document.getElementsByTagName("body");
    body[0].appendChild(canvas);

    let ctx = canvas.getContext("2d");
    if (ctx === null) {
        throw new Error("getContext('2d')")
    }

    const root = new Panel(0, 0);
    root.setBgColor(color.Name("blue"));
    root.setPos(0, 0);
    root.setBorderColor(color.Name("black"));
    root.setBorder(1, 1, 1, 1);
    root.setBgColor(color.Name("whitesmoke"));
    root.setPadding(10, 10, 10, 10);
    root.setSize(canvas.width, canvas.height);

    const pl = new Panel(400, 600);
    pl.setBorderColor(color.Name("black"));
    pl.setBorder(1, 1, 1, 1);
    pl.setBgColor(color.Name("white"));
    const lt = new vbox.VBox(pl);
    pl.setLayout(lt);
    root.add(pl);

    const b1 = button.New("Add child");
    b1.setPos(0, 0);
    b1.update();
    b1.subscribe(event.Click, (/*ev*/) => {
        // Create button
        const text = "Child" + pl.children().length.toString();
        const b = button.New(text);
        let height = b.height();
        b.setHeight(Math.floor(height + 100 * Math.random()));
        // Sets the button layout parameters
        const params = <vbox.Params>vbox.VBox.childParams();
        if (rleft.value()) {
            params.align = vbox.AlignChild.Left;
        } else 
        if (rcenter.value()) {
            params.align = vbox.AlignChild.Center;
        } else 
        if (rright.value()) {
            params.align = vbox.AlignChild.Right;
        } else
        if (rwidth.value()) {
            params.align = vbox.AlignChild.Width;
        }
        if (cexpand.value()) {
            params.expand = 1;
        }
        b.setLayoutParams(params);
        //console.log("params:", b.layoutParams());
        pl.add(b);
    });
    root.add(b1);

    const groupV = "alignV";
    const rleft = radio.New(vbox.AlignChild.Left, groupV);
    rleft.setValue(true);
    rleft.setPos(b1.posX() + b1.width() + 5, b1.posY());
    rleft.update();
    root.add(rleft);

    const rcenter = radio.New(vbox.AlignChild.Center, groupV);
    rcenter.setPos(rleft.posX() + rleft.width() + 5, rleft.posY());
    rcenter.update();
    root.add(rcenter);

    const rright = radio.New(vbox.AlignChild.Right, groupV);
    rright.setPos(rcenter.posX() + rcenter.width() + 5, rcenter.posY());
    rright.update();
    root.add(rright);

    const rwidth = radio.New(vbox.AlignChild.Width, groupV);
    rwidth.setPos(rright.posX() + rright.width() + 5, rright.posY());
    rwidth.update();
    root.add(rwidth);

    const cexpand = checkbox.New("Expand");
    cexpand.setPos(rwidth.posX() + rwidth.width() + 5, rwidth.posY());
    cexpand.update();
    root.add(cexpand);

    const bRem = button.New("Remove");
    bRem.setPos(cexpand.posX() + cexpand.width() + 20, 0);
    bRem.update();
    root.add(bRem);
    bRem.subscribe(event.Click, () => {
        const nchildren = pl.children().length;
        if (nchildren == 0) {
            return;
        }
        const child = pl.children()[nchildren-1];
        pl.remove(child);
    });

    const setAlignH = (align: string) => {
        lt.setAlign(<vbox.AlignGroup>align);
    };

    const groupH = "alignH";
    const rtop = radio.New(vbox.AlignGroup.Top, groupH);
    rtop.setValue(true);
    rtop.setPos(b1.posX(), b1.posY() + b1.height() + 5);
    rtop.update();
    rtop.subscribe(event.Click, ()=>setAlignH(rtop.text()));
    root.add(rtop);

    const rcenterh = radio.New(vbox.AlignGroup.Center, groupH);
    rcenterh.setPos(rtop.posX() + rtop.width() + 5, rtop.posY());
    rcenterh.update();
    rcenterh.subscribe(event.Click, ()=>setAlignH(rcenterh.text()));
    root.add(rcenterh);

    const rbottom = radio.New(vbox.AlignGroup.Bottom, groupH);
    rbottom.setPos(rcenterh.posX() + rcenterh.width() + 5, rcenterh.posY());
    rbottom.update();
    rbottom.subscribe(event.Click, ()=>setAlignH(rbottom.text()));
    root.add(rbottom);

    const rheight = radio.New(vbox.AlignGroup.Height, groupH);
    rheight.setPos(rbottom.posX() + rbottom.width() + 5, rbottom.posY());
    rheight.update();
    rheight.subscribe(event.Click, ()=>setAlignH(rheight.text()));
    root.add(rheight);

    const cautoHeight = checkbox.New("AutoHeight");
    cautoHeight.setPos(rheight.posX() + rheight.width() + 10, rheight.posY());
    cautoHeight.update();
    cautoHeight.subscribe(event.Click, ()=>lt.setAutoHeight(cautoHeight.value()));
    root.add(cautoHeight);

    const cautoWidth = checkbox.New("AutoWidth");
    cautoWidth.setPos(cautoHeight.posX() + cautoHeight.width() + 5, cautoHeight.posY());
    cautoWidth.update();
    cautoWidth.subscribe(event.Click, ()=>lt.setAutoWidth(cautoWidth.value()));
    root.add(cautoWidth);

    pl.setPos(0, cautoWidth.posY() + cautoWidth.height() + 5);

    // Starts rendering
    const rend = renderer.New(canvas, root);
    rend.start();

    // Starts event manager
    evm.get().add(canvas, root);
    evm.get().start();
}

tests.All.set("vbox_layout", run);
