import {Panel} from "./panel";
import {Dispatcher} from "./dispatcher";
import * as event from "./event";

// Instance of EventManager singleton
var instance: EventManager

// Returns the instance of the EventManager singleton
export function get() {

    if (instance !== undefined) {
        return instance;
    }
    instance = Object.seal(new EventManager());
    return instance;
}

export class EventManager extends Dispatcher {

    private _canvases:      Map<HTMLElement, any>;
    private _started:       boolean;
    private _mouseFocus:    Panel|null;
    private _keyFocus:      Panel|null;
    private _modalPanel:    Panel|null;
    private _cbKeydown:     (ev: any) => void;
    private _cbKeyup:       (ev: any) => void;
    private _cbKeypress:    (ev: any) => void;

    constructor() {

        super();
        this._canvases = new Map();                 // maps canvas to state containing associated panel and callbacks.
        this._started = false;                      // manager started state
        this._mouseFocus = null;                    // current panel with the mouse focus (in any canvas)
        this._keyFocus = null;                      // current panel with the key focus (in any canvas)
        this._modalPanel = null;                    // current modal panel
        this._cbKeydown = (ev) => this._onKey(ev);  // keydown callback
        this._cbKeyup = (ev) => this._onKey(ev);    // keyup callback
        this._cbKeypress = (ev) => this._onKey(ev); // keypress callback
    }

    // Add a canvas and panel to be managed by the event manager.
    add(canvas: HTMLElement, panel: object) {

        let state = this._canvases.get(canvas);
        if (state === undefined) {
            state = Object.freeze({cb:{}, panel:panel});
            this._canvases.set(canvas, state);
        } else {
            state.panel = panel;
        }
    }

    // Remove the specified canvas from the event manager.
    remove(canvas: HTMLCanvasElement) {

        let state = this._canvases.get(canvas);
        if (state === undefined) {
            return;
        }
        this._removeListeners(canvas);
    }


    // Starts the event manager which add event listeners and dispatch
    // received events to the managed panels.
    start() {

        if (this._started) {
            return;
        }
        window.addEventListener(event.KeyDown, this._cbKeydown);
        window.addEventListener(event.KeyUp, this._cbKeyup);
        window.addEventListener(event.KeyPress, this._cbKeypress);
        for (let canvas of this._canvases.keys()) {
            this._addListeners(canvas);
        }
        this._started = true;
    }

    // Stops the event manager which removes previously installed event listeners
    // and stops dispatching events to the managed panels.
    stop() {

        if (!this._started) {
            return;
        }
        window.removeEventListener(event.KeyDown, this._cbKeydown);
        window.removeEventListener(event.KeyUp, this._cbKeyup);
        window.removeEventListener(event.KeyPress, this._cbKeypress);
        for (let canvas of this._canvases.keys()) {
            this._removeListeners(canvas);
        }
        this._started = false;
    }

    // Clears the key focus panel (if any) without
    // sending OnLostKeyFocus for previous focused panel
    clearKeyFocus() {

        this._keyFocus = null;
    }

    // Sets the panel which will receive all keyboard events
    // Passing null will remove the focus (if any)
    setKeyFocus(panel: Panel|null) {
        
        if (this._keyFocus !== null) {
            // If this panel is already in focus, nothing to do
            if (panel !== null && this._keyFocus === panel) {
                return;
            }
            // Sends event to panel which has lost focus
            this._keyFocus._onEvent(event.LostKeyFocus, {type:event.LostKeyFocus});
        }
        this._keyFocus = panel;
    }

    // Sets the panel which will receive all mouse events
    // Passing null will restore the default event processing
    setMouseFocus(panel: Panel|null) {

        this._mouseFocus = panel;
    }

    // Sets the modal panel.
    // If there is a modal panel, only events for this panel are dispatched
    // To remove the modal panel call this function with a nulll panel.
    setModal(panel: Panel) {
    
        this._modalPanel = panel;
    }

    //// Sets the panel which will receive all scroll events
    //// Passing nil will restore the default event processing
    //setWheelFocus(panel) {
    //
    //  r.scrollFocus = ipan
    //}

    _addListeners(canvas: HTMLElement) {

        const state = this._canvases.get(canvas);
        state.cb["mousedown"] = (ev: any) => {
            this._onMouse(ev, canvas, state.panel);
        };
        canvas.addEventListener(event.MouseDown, state.cb["mousedown"]);
        state.cb["mouseup"] = (ev: MouseEvent) => {
            this._onMouse(ev, canvas, state.panel);
        };
        canvas.addEventListener(event.MouseUp, state.cb["mouseup"]);
        state.cb["mousemove"] = (ev: MouseEvent) => {
            this._onMouse(ev, canvas, state.panel);
        };
        canvas.addEventListener(event.MouseMove, state.cb["mousemove"]);
        state.cb["wheel"] = (ev: MouseEvent) => {
            this._onMouse(ev, canvas, state.panel);
        };
        canvas.addEventListener(event.Wheel, state.cb["wheel"]);
    }

    _removeListeners(canvas: HTMLElement) {

        const state = this._canvases.get(canvas);
        canvas.removeEventListener(event.MouseDown, state.cb["mousedown"]);
        canvas.removeEventListener(event.MouseUp, state.cb["mouseup"]);
        canvas.removeEventListener(event.MouseMove, state.cb["mousemove"]);
        canvas.removeEventListener(event.Wheel, state.cb["wheel"]);
    }

    // Returns mouse position relative to the canvas for the specified event
    _mousePos(ev: MouseEvent, canvas: HTMLElement) { 

        const rect = canvas.getBoundingClientRect();
        return {
            x: ev.clientX - rect.left,
            y: ev.clientY - rect.top
        };
    }

    // Receives "mousedown", "mouseup", "mousemove" and "wheel" events
    // and dispatch them to the panels under the mouse position.
    _onMouse(ev: any, canvas: HTMLElement, panel: Panel) {

        const rect = canvas.getBoundingClientRect();
        ev.canvasX = ev.clientX - rect.left;
        ev.canvasY = ev.clientY - rect.top;
        this._sendPanels(ev, panel);
    }

    // Called when any key event is received
    _onKey(ev: KeyboardEvent) {

        // If no panel has the key focus, nothing to do
        if (this._keyFocus === null) {
            return;
        }
        // Checks modal panel
        if (!this._canDispatch(this._keyFocus)) {
            return;
        }

        // Dispatch key event to focused panel
        this._keyFocus._onEvent(ev.type, ev);
    }

    // Returns if event can be dispatched to the specified panel.
    // An event cannot be dispatched if there is a modal panel and the specified
    // panel is not the modal panel or any of its children.
    _canDispatch(panel: Panel) {
    
        if (this._modalPanel === null) {
            return true;
        }
        if (this._modalPanel === panel) {
            return true;
        }
        if (this._modalPanel.isDescendant(panel)) {
            return true;
        }
        return false;
    }

    // Sends a mouse event to the currently focused panel or panels
    // which contain the specified canvas position
    _sendPanels(ev: any, root: Panel) {

        // If there is panel with mouse focus send only to this panel
        if (this._mouseFocus !== null) {
            // Checks modal panel
            if (!this._canDispatch(this._mouseFocus)) {
                return;
            }
            this._sendToMouseFocus(ev, this._mouseFocus);
            return;
        }

        // list of panels which contains the mouse position
        const targets: Panel[] = [];

        // Checks recursively if the specified panel and
        // any of its children contain the mouse position
        const checkPanel = (p: Panel) => {

            // If panel not visible or not enabled, ignore
            if (!p.visible() || !p.enabled()) {
                return;
            }
            // Checks if this panel contains the mouse position
            const found = p.insideBorders(ev.canvasX, ev.canvasY);
            if (found) {
                targets.push(p);
            }
            else {
                // If OnMouseEnter previously sent, sends OnMouseLeave with a nil event
                if (p._mouseEnter) {
                    p._onEvent(event.MouseLeave, {type: event.MouseLeave});
                    p._mouseEnter = false;
                }
                // If mouse button was pressed, sends event informing mouse down outside of the panel
                if (ev.type === event.MouseDown) {
                    p._onEvent(event.MouseOut, {type: event.MouseOut});
                }
            }
            // Checks if any of its children also contains the position
            p.children().forEach(checkPanel);
        };

        // Checks the root panel and all of its children building targets list.
        checkPanel(root);

        // No panels found
        if (targets.length === 0) {
            // If event is mouse click, removes the keyboard focus
            if (ev.type === event.MouseDown) {
                this.setKeyFocus(null);
            }
            return;
        }

        // Sorts panels by absolute z with the most foreground (greater Z) panels first
        const compareFn = (a:Panel, b:Panel) => {

            if (a.posZ() < b.posZ()) {
                return -1;
            }
            if (a.posZ() > b.posZ()) {
                return 1;
            }
            return 0;
        };
        targets.sort(compareFn);

        // Send events to panels
        let stop = false;
        for (const p of targets) {
            // Checks for modal panel
            if (!this._canDispatch(p)) {
                continue;
            }
            // Mouse move event
            if (ev.type === event.MouseMove) {
                stop = p._onEvent(event.MouseMove, ev);
                if (!p._mouseEnter) {
                    stop = p._onEvent(event.MouseEnter, {type: event.MouseEnter});
                    p._mouseEnter = true;
                }
                // Mouse button/wheel event
            } else {
                stop = p._onEvent(ev.type, ev);
            }
            if (stop) {
                break;
            }
        }
    }

    // Sends mouse event to panel with mouse focus
    _sendToMouseFocus(ev: any, p: Panel) {

        // Mouse move event
        if (ev.type === event.MouseMove) {
            p._onEvent(event.MouseMove, ev);
            // Checks if this panel contains the mouse position
            const inside = p.insideBorders(ev.canvasX, ev.canvasY);
            if (inside) {
                if (!p._mouseEnter) {
                    p._onEvent(event.MouseEnter, {type: event.MouseEnter});
                    p._mouseEnter = true;
                    return;
                }
            } else {
                if (p._mouseEnter) {
                    p._onEvent(event.MouseLeave, {type: event.MouseLeave});
                    p._mouseEnter = false;
                }
            }
        }
        // Mouse button/wheel event
        p._onEvent(ev.type, ev);
    }
}

