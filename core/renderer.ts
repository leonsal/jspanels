"use strict";

import * as dispatcher from "./dispatcher";
import * as logger from "../util/logger";
import * as panel from "./panel";

export const OnRender = "OnRender";

export function New(canvas: HTMLCanvasElement, pan: panel.Panel) {

    return Object.seal(new Renderer(canvas, pan));
}

export class Renderer extends dispatcher.Dispatcher {

    private _canvas:    HTMLCanvasElement;
    private _panel:     panel.Panel;
    private _ctx:       CanvasRenderingContext2D;
    private _started:   boolean;
    private _log:       logger.Logger;

    constructor(canvas: HTMLCanvasElement, pan: panel.Panel) {

        super();
        this._canvas = canvas;
        this._panel = pan;
        const ctx = canvas.getContext("2d");
        if (!ctx) {
            throw new Error("Couldn't get canvas context 2d");
        }
        this._ctx = ctx;
        this._started = false;
        this._log = logger.getLogger("Renderer");
    }

    // Starts the rendering of the panel in the canvas
    // if not already started.
    start() {

        if (this._started) {
            return;
        }
        this._started = true;

        // Callback for requestAnimationFrame
        const cb = (ts: number) => {
            if (!this._started) {
                return;
            }
            // Dispatch event before render
            //this.dispatch(OnRender, ts);
            // If panel or any descendent changed, render it
            if (this._panel.treeChanged()) {
                const start = performance.now();
                this._render(this._panel, ts);
                //this._log.debug("render: %5.2fms", performance.now() - start);
            }
            window.requestAnimationFrame(cb);
        };
        window.requestAnimationFrame(cb);
    }

    stop() {

        this._started = false;
    }

    // Render the specified panel and all its children
    _render(p: panel.Panel, ts:number) {

        p.render(this._ctx);
        for (const child of p.children()) {
            this._render(child, ts);
        }
    }
}


