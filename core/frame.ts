import {Panel} from "./panel";
import {Renderer} from "./renderer";
import * as evm from "./event_manager";

export class Frame {

    _canvas: HTMLCanvasElement;
    _ctx: CanvasRenderingContext2D;
    _base: HTMLDivElement;
    _div:  HTMLDivElement;
    _root: Panel;
    _rend: Renderer;
    _started: boolean;

    constructor(width: number, height: number) {

        // Creates canvas element
        this._canvas = document.createElement("canvas");
        const ctx = this._canvas.getContext("2d");
        if (ctx == null) {
            throw new Error("getContext()");
        }
        this._ctx = ctx;
        this._canvas.style.position = "absolute";
        this._root = new Panel(width, height);

        // Creates base HTML Div element
        this._base = document.createElement("div");
        this._base.style.position = "relative";

        // Created div element for user components
        this._div = document.createElement("div");
        this._div.style.position = "absolute";

        // Append elements to base div
        this._base.appendChild(this._canvas);
        this._base.appendChild(this._div);

        this._rend = new Renderer(this._canvas, this._root);
        this._started = false;
        this._setSize(width, height);
    }

    htmlElement(): HTMLElement {

        return this._base;
    }

    /** Returns the root panel of this frame */
    root() :Panel {

        return this._root;
    }

    div(): HTMLDivElement {

        return this._div;
    }

    canvas() {
        return this._canvas;
    }

    start() {
        if (this._started) {
            return;
        }
        this._rend.start();
        evm.get().add(this._div, this._root);
        evm.get().start();
        this._started = true;
    }

    stop() {
        if (!this._started) {
            return;
        }
        this._rend.stop();
        evm.get().stop();
        this._started = false;
    }

    _setSize(width: number, height: number) {

        this._canvas.width = width;
        this._canvas.height = height;
        this._root.setSize(width, height);
        this._div.style.width = `${width}px`;
        this._div.style.height = `${height}px`;
    }

}