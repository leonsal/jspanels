// Event names
export const Click           = "OnClick";         // generated event
export const Change          = "OnChange";        // generated event
export const LostKeyFocus    = "OnLostKeyFocus";  // generated event
export const KeyDown         = "keydown";         // Javascript native event
export const KeyUp           = "keyup";           // Javascript native event
export const KeyPress        = "keypress";        // Javascript native event
export const MouseDown       = "mousedown";       // Javascript native event
export const MouseUp         = "mouseup";         // Javascript native event
export const MouseEnter      = "OnMouseEnter";    // generated event
export const MouseLeave      = "OnMouseLeave";    // generated event
export const MouseMove       = "mousemove";       // Javascript native event
export const MouseOut        = "OnMouseOut";      // generated event
export const Resize          = "OnResize";        // generated event 
export const Wheel           = "wheel";           // Javacript native event
export const Group           = "OnGroup";         // generated by radio buttons.

export interface MouseEventCanvas extends MouseEvent {
    canvasX:    number,
    canvasY:    number;
}