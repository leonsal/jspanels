export class Border {

    top:    number;
    right:  number;
    bottom: number;
    left:   number;

    constructor(top=0, right=0, bottom=0, left=0) {

        this.top = top;
        this.right = right;
        this.bottom = bottom;
        this.left = left;
    }

    clone() {

        return new Border(this.top, this.right, this.bottom, this.left);
    }

    set(top: number, right: number, bottom: number, left: number) {
        
        this.top = top;
        this.right = right;
        this.bottom = bottom;
        this.left = left;
    }

    setFrom(src: { top: number; right: number; bottom: number; left: number; }) {

        this.top = src.top;
        this.right = src.right;
        this.bottom = src.bottom;
        this.left = src.left;
    }

    isEmpty() {

        if (this.top === 0 && this.right === 0 && this.bottom === 0 && this.left === 0) {
            return true;
        }
        return false;
    }
}


export class Size {

    x:      number;
    y:      number;
    width:  number;
    height: number;

    constructor(x=0, y=0, width=0, height=0) {

        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
}


