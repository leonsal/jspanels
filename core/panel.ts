import * as color from "./color";
import * as rect  from "./rect";
import * as dispatcher from "./dispatcher";
import * as layout from "../layout/layout";
import * as event from "./event";


export class Panel extends dispatcher.Dispatcher {

    protected _width:         number;
    protected _height:        number;
    protected _x:             number;
    protected _y:             number;
    protected _z:             number;
    protected _parent:        Panel|null;
    protected _children:      Panel[];
    protected _margin:        rect.Border;
    protected _border:        rect.Border;
    protected _padding:       rect.Border;
    protected _borderRadius:  number;
    protected _content:       rect.Size;
    protected _marginColor:   color.Color;
    protected _borderColor:   color.Color;
    protected _paddingColor:  color.Color;
    protected _contentColor:  color.Color;
    protected _visible:       boolean;
    protected _bounded:       boolean;
    protected _enabled:       boolean;
    protected _changed:       boolean;
    public    _mouseEnter:    boolean;
    protected _xabs:          number;
    protected _yabs:          number;
    protected _xabsMin:       number;
    protected _xabsMax:       number;
    protected _yabsMin:       number;
    protected _yabsMax:       number;
    protected _layout:        layout.ILayout|null;
    protected _layoutParams:  layout.IParams|null;

    constructor(width=0, height=0) {

        super();
        this._width = width;                        // external panel width
        this._height = height;                      // external panel height
        this._x = 0;                                // panel top left x coordinate relative to parent
        this._y = 0;                                // panel top left y coordinate relative to parent
        this._z = 0;                                // panel z order
        this._parent = null;                        // parent panel
        this._children = [];                        // list of children panels
        this._margin = new rect.Border();           //
        this._border = new rect.Border();           //
        this._padding = new rect.Border();          //
        this._borderRadius = 0;                     // radius for rounded borders
        this._content = new rect.Size();            //
        this._marginColor = color.RGBA();            // current margin color
        this._borderColor = color.RGBA();            // current border color
        this._paddingColor = color.RGBA();           // current padding color
        this._contentColor = color.RGBA();           // current content color
        this._visible = true;                       // visibility state
        this._bounded = true;                       // indicates if panel is bounded by its parent or not
        this._enabled = true;                       // enable/disable event processing
        this._changed = true;                       // indicates if panel was changed after last render
        this._mouseEnter = false;                   // indicates if mouse enter event was received
        this._xabs    = 0;                          // absolute x coordinate
        this._yabs    = 0;                          // absolute y coordinate
        this._xabsMin = 0;                          // absolute minimum x coordinate
        this._xabsMax = 0;                          // absolute maximum x coordinate
        this._yabsMin = 0;                          // absolute minimum y coordinate
        this._yabsMax = 0;                          // absolute maximum y coordinate
        this._layout  = null;                       // optional layout manager instance
        this._layoutParams = null;                   // optional layout child parameters
        this._resize(width, height);
    }

    // Returns the absolute position of this panel content area in the last canvas it was rendered
    absContentPos() {

        const xabs = this._xabs + this._margin.left + this._border.left + this._padding.left;
        const yabs = this._yabs + this._margin.top + this._border.top + this._padding.top;
        return {x:xabs, y:yabs};
    }

    // AbsPos returns the absolute position of this panel in the last canvas it was rendered.
    absPos() {

        return {x:this._xabs, y:this._yabs};
    }

    // Add adds the specified panel to the list of this panel's children and sets its parent pointer.
    // If the specified panel had a parent, the specified panel is removed from the original parent's list of children.
    add(child: Panel) {

        if (child === this) {
            throw("Panel can't be added as a child of itself");
        }

        // If the specified child already has a parent,
        // remove it from the original parent's list of children
        if (child._parent !== null) {
            child._parent.remove(child);
        }

        child._parent = this;
        this._children.push(child);
        // If panel has layout and child being added don't have layout parameters,
        // sets the child default layout parameters.
        if (this._layout !== null) {
            if (child._layoutParams === null) {
                child._layoutParams = this._layout.childParams();
            }
            this._layout.update();
        }
        this._changed = true;
        return this;
    }

    // Applies the properties in the specified style object to this panel margin, border, padding and bgcolor.
    applyStyle(style: any) {

        if (style.margin) {
            this.setMarginFrom(style.margin);
        }
        if (style.border) {
            this.setBorderFrom(style.border);
        }
        if (style.borderRound) {
            this.setBorderRound(style.borderRound[0], style.borderRound[1]);
        }
        if (style.padding) {
            this.setPaddingFrom(style.padding);
        }
        if (style.marginColor) {
            this.setMarginColor(style.marginColor);
        }
        if (style.borderColor) {
            this.setBorderColor(style.borderColor);
        }
        if (style.paddingColor) {
            this.setPaddingColor(style.paddingColor);
        }
        if (style.bgColor) {
            this.setBgColor(style.bgColor);
        }
    }

    // Borders returns this panel current border sizes
    borders() {

        return this._border.clone();
    }

    // Bounded returns this panel bounded state
    bounded() {

        return this._bounded;
    }

    // Children returns the list of this panel's children.
    children() {

        return this._children;
    }

    // Changed returns if this panel was changes after its last render
    changed() {

        return this._changed;
    }

    // Clip sets a clip region in the specified canvas to this panel content area
    clipContent(ctx: CanvasRenderingContext2D) {
    
        const x = Math.max(this._xabsMin, this._xabs+this._calcLeftBorders());
        const y = Math.max(this._yabsMin, this._yabs+this._calcTopBorders());
        const width = Math.min(this._content.width, this._xabsMax-this._calcRightBorders()-this._xabsMin);
        const height = Math.min(this._content.height, this._yabsMax-this._calcBottomBorders()-this._yabsMin);
        ctx.beginPath();
        ctx.rect(x, y, width, height);
        ctx.clip();

        // If border is rounded adds another clipping region inside the border,
        // because depending on the radius the content could spill out of the border.
        if (this._borderRadius > 0) {
            const xBorder = this._xabs + this._margin.left;
            const yBorder = this._yabs + this._margin.top;
            this._setBorderPath(ctx, xBorder, yBorder);
            ctx.clip();
        }
    }

    // ContainsPosition returns indication if this panel contains
    // the specified absolute canvas position in pixels.
    containsPosition(x: number, y: number) {
    
        if (x < this._xabs || x >= (this._xabs+this._width)) {
            return false;
        }
        if (y < this._yabs || y >= (this._yabs+this._height)) {
            return false;
        }
        return true;
    }

    // ContentHeight returns the current height of the content area in pixels
    contentHeight() {
    
        return this._content.height;
    }

    // ContentWidth returns the current width of the content area in pixels
    contentWidth() {
    
        return this._content.width;
    }

    // Enabled returns the current enabled state of this panel
    enabled()  {
    
        return this._enabled;
    }
    
    // Height returns the current panel external height in pixels
    height() {
    
        return this._height;
    }

    // Returns true if the specified screen position in pixels is inside the panel borders,
    // including the borders width.
    // Unlike "containsPosition" is does not consider the panel margins.
    insideBorders(x: number, y: number) {

        if (x < (this._xabs+this._margin.left) || x >= (this._xabs +this._width -this._margin.right) ||
            y < (this._yabs+this._margin.top)  || y >= (this._yabs +this._height-this._margin.bottom)) {
            return false;
        }
        return true;
    }

    // Checks if the specified panel is a child of this panel or of any
    // of its children recursively.
    isDescendant(panel: Panel) {

        for (const child of this._children) {
            if (child === panel) {
                return true;
            }
            if (this.isDescendant(child)) {
                return true;
            }
        }
        return false;
    }

    // Returns the instance of this panel current layout
    layout() {

        return this._layout;
    }

    // Returns object with the layout child parameters
    layoutParams() {

        return this._layoutParams;
    }

    // Margins returns the current margin sizes in pixels
    margins() {
    
        return this._margin.clone();
    }

    // Paddings returns this panel padding sizes in pixels
    paddings() {
    
        return this._padding.clone();
    }

    // Parent returns this panel's parent panel
    parent() {
    
        return this._parent;
    }

    pos() {

        return {x: this._x, y: this._y, z: this._z};
    }

    posX() {

        return this._x;
    }

    posY() {

        return this._y;
    }

    posZ() {

        return this._z;
    }

    update() {

    }

    // Remove removes the specified child from the list of this panel's children.
    // Returns true if found or false otherwise.
    remove(child: Panel) {
   
        for (let pos = 0; pos < this._children.length; pos++) {
            if (child === this._children[pos]) {
                this._children.splice(pos, 1);
                child._parent = null;
                this._changed = true;
                if (this._layout !== null) {
                    this._layout.update();
                }
                return true;
            }
        }
        return false;
    }

    // Render draws this panel on the specified canvas
    render(ctx: CanvasRenderingContext2D) {

        if (this._borderRadius === 0) {
            this._renderRect(ctx);
        } else {
            this._renderRounded(ctx);
        }
    }


    // Sets this panel border sizes in pixels
    // and recalculates the panel external size
    setBorder(top: number, right:number, bottom: number, left: number) {
   
        this._borderRadius = 0;
        this._border.set(top, right, bottom, left);
        this._resize(this._calcWidth(), this._calcHeight(), true);
    }

    // Sets this panel border sizes from the specified
    // RectSizes object and recalculates the panel external size
    setBorderFrom(src: rect.Border) {
    
        this._borderRadius = 0;
        this._border.setFrom(src);
        this._resize(this._calcWidth(), this._calcHeight(), true);
    }

    // Sets the color of this panel border from the specified color.
    setBorderColor(color: color.Color) {
    
        this._borderColor.setFrom(color);
        this._changed = true;
    }

    // Sets the border rounded with the specified radius and border width.
    // Overrides previous values by setBorder() or setBorderFrom();
    setBorderRound(radius=0, width=0) {
    
        this._borderRadius = radius;
        this._border.set(width, width, width, width);
        this._changed = true;
    }

    // Sets this panel bounded state
    setBounded(bounded: boolean) {
    
        this._bounded = bounded;
        this._changed = true;
    }

    // setChanged sets this panel changed flag
    setChanged(changed: boolean) {
    
        this._changed = changed;
    }

    // Sets the height of the content area of the panel
    // to the specified value and adjusts its width to keep the same aspect ratio.
    setContentAspectHeight(height: number) {
    
        const aspect = this._content.width / this._content.height;
        const width = height / aspect;
        this.setContentSize(width, height);
    }

    // Sets the width of the content area of the panel
    // to the specified value and adjusts its height to keep the same aspect radio.
    setContentAspectWidth(width: number) {
    
        const aspect = this._content.width / this._content.height;
        const height = width / aspect;
        this._setContentSize(width, height);
    }

    // Sets this panel content height to the specified dimension in pixels.
    // The external size of the panel may increase or decrease to accommodate the new width
    setContentHeight(height: number) {
    
        this._setContentSize(this._content.width, height);
    }
    
    // Sets this panel content size to the specified dimensions.
    // The external size of the panel may increase or decrease to acomodate
    // the new content size.
    setContentSize(width: number, height: number) {
    
        this._setContentSize(width, height, true);
    }
    
    // SetContentWidth sets this panel content width to the specified dimension in pixels.
    // The external size of the panel may increase or decrease to accommodate the new width
    setContentWidth(width: number) {
    
        this._setContentSize(width, this._content.height);
    }

    // Sets the panel enabled state
    // A disabled panel do not process key or mouse events.
    setEnabled(state: boolean) {
    
        this._enabled = state;
        this._changed = true;
        //p.Dispatch(OnEnable, nil)
    }

    // Sets this panel external height in pixels.
    // The internal panel areas and positions are recalculated
    setHeight(height: number) {
    
        this.setSize(this._width, height);
    }

    // Sets this panel layout. Pass null to remove the layout.
    setLayout(layout: layout.ILayout) {

        this._layout = layout;
        this._changed = true;
    }

    // Sets this panel layout child parameters
    setLayoutParams(params: layout.IParams|null) {

        this._layoutParams = params;
    }

    // Sets this panel margin sizes in pixels
    // and recalculates the panel external size
    setMargin(top: number, right: number, bottom: number, left: number) {
    
        this._margin.set(top, right, bottom, left);
        this._resize(this._calcWidth(), this._calcHeight(), true);
    }

    // Sets this panel margins sizes from the specified
    // RectBounds object and recalculates the panel external size
    setMarginFrom(src: rect.Border) {
    
        this._margin.setFrom(src);
        this._resize(this._calcWidth(), this._calcHeight(), true);
    }

    // Sets the color of this panel margins.
    // By default the margins are transparent.
    setMarginColor(color: color.Color) {
    
        this._marginColor.setFrom(color);
        this._changed = true;
    }

    // Sets the panel padding sizes in pixels
    setPadding(top: number, right: number, bottom: number, left: number) {
    
        this._padding.set(top, right, bottom, left);
        this._resize(this._calcWidth(), this._calcHeight(), true);
    }

    // Sets this panel padding sizes from the specified
    // RectBounds pointer and recalculates the panel size
    setPaddingFrom(src: rect.Border) {
    
        this._padding.setFrom(src);
        this._resize(this._calcWidth(), this._calcHeight(), true);
    }

    // Sets the color of this panel paddings.
    setPaddingColor(color: color.Color) {
    
        this._paddingColor.setFrom(color);
        this._changed = true;
    }

    // Sets this panel absolute position in pixel coordinates
    // from left to right and from top to bottom of the canvas.
    setPos(x: number, y: number) {
    
        this._x = Math.round(x);
        this._y = Math.round(y);
        this._changed = true;
    }

    // Sets the color of the panel paddings and content area
    // The padding/content opacity is set to 1.0 (full opaque)
    setBgColor(color: color.Color) {
    
        this._paddingColor.setFrom(color);
        this._contentColor.setFrom(color);
        this._changed = true;
        return this;
    }

    // Sets this panel external width and height in pixels.
    setSize(width: number, height: number) {
    
        if (width < 0) {
            width = 0;
        }
        if (height < 0) {
            height = 0;
        }
        this._resize(width, height, true);
    }

    // Sets this panel visibility state.
    setVisible(visible: boolean) {
    
        this._visible = visible;
        this._changed = true;
    }

    // Sets this panel external width in pixels.
    // The internal panel areas and positions are recalculated
    setWidth(width: number) {
    
        this.setSize(width, this._height);
    }

    // Size returns this panel current external width and height in pixels
    size() {
    
        return {width:this._width, height:this._height};
    }

    // Returns if this panel of any of its descendants has changed
    treeChanged() {

        if (this._changed) {
            return true;
        }
        for (const child of this._children) {
            if (child.treeChanged()) {
                return true;
            }
        }
        return false;
    }

    // Returns this panel's visibility state
    visible() {
    
        return this._visible;
    }

    // Width returns the current panel external width in pixels
    width() {
    
        return this._width;
    }

    // calcBorderWidth calculates this panel border rectangle width in pixels
    _calcBorderWidth() {
    
        return this._content.width +
            this._padding.left + this._padding.right +
            this._border.left + this._border.right;
    }

    // calcBorderHeight calculates this panel border rectangle height in pixels
    _calcBorderHeight() {
    
        return this._content.height +
            this._padding.top + this._padding.bottom +
            this._border.top + this._border.bottom;
    }

    _calcLeftBorders() {
    
        return this._margin.left + this._border.left + this._padding.left;
    }

    _calcRightBorders() {
    
        return this._margin.right + this._border.right + this._padding.right;
    }

    _calcTopBorders() {
    
        return this._margin.top + this._border.top + this._padding.top;
    }

    _calcBottomBorders() {
    
        return this._margin.bottom + this._border.bottom + this._padding.bottom;
    }

    // Calculates this panel padding rectangle width in pixels
    _calcPaddingWidth() {
    
        return this._content.width +
            this._padding.left + this._padding.right;
    }

    // Calculates this panel padding rectangle height in pixels
    _calcPaddingHeight() {
    
        return this._content.height +
            this._padding.top + this._padding.bottom;
    }

    // Calculates the panel external width in pixels
    _calcWidth() {
    
        return this._content.width +
            this._padding.left + this._padding.right +
            this._border.left + this._border.right +
            this._margin.left + this._margin.right;
    }
    
    // Calculates the panel external height in pixels
    _calcHeight() {
    
        return this._content.height +
            this._padding.top + this._padding.bottom +
            this._border.top + this._border.bottom +
            this._margin.top + this._margin.bottom;
    }

    // Tries to set the external size of the panel to the specified
    // dimensions and recalculates the size and positions of the internal areas.
    // The margins, borders and padding sizes are kept and the content
    // area size is adjusted. So if the panel is decreased, its minimum
    // size is determined by the margins, borders and paddings.
    // Normally it should be called with dispatch=true to recalculate the
    // panel layout and dispatch OnSize event.
    _resize(width: number, height: number, dispatch=false) {

        // Adjusts content width
        this._content.width = width -
            this._margin.left - this._margin.right -
            this._border.left - this._border.right -
            this._padding.left - this._padding.right;
        if (this._content.width < 0) {
            this._content.width = 0;
        }
    
        // Adjusts content height
        this._content.height = height -
            this._margin.top - this._margin.bottom -
            this._border.top - this._border.bottom -
            this._padding.top - this._padding.bottom;
        if (this._content.height < 0) {
            this._content.height = 0;
        }
    
        // Sets final external size
        this._width = this._calcWidth();
        this._height = this._calcHeight();
        this._changed = true;
   
        // Update layout and dispatch event
        if (!dispatch) {
            return;
        }
        if (this._layout !== null) {
            this._layout.update();
        }
        this.dispatch(event.Resize);
    }

    // Internal version of SetContentSize() which allows
    // to determine if the panel will recalculate its layout and dispatch event.
    // It is normally used by layout managers when setting the panel content size
    // to avoid another invokation of the layout manager.
    _setContentSize(width: number, height: number, dispatch=false) {
   
        width = Math.round(width);
        height = Math.round(height);
        // Calculates the new desired external width and height
        const eWidth = width +
            this._padding.left + this._padding.right +
            this._border.left + this._border.right +
            this._margin.left + this._margin.right;
        const eHeight = height +
            this._padding.top + this._padding.bottom +
            this._border.top + this._border.bottom +
            this._margin.top + this._margin.bottom;
        this._resize(eWidth, eHeight, dispatch);
    }

    // setParentOf is used by Add and AddAt.
    // It verifies that the node is not being added to itself and sets the parent pointer of the specified node.
    // If the specified node had a parent, the specified node is removed from the original parent's list of children.
    // It does not add the specified node to the list of children.
    setParentOf(child: Panel) {
    
        if (this === child) {
            throw("Panel.{Add,AddAt}: object can't be added as a child of itself");
        }
        // If the specified child already has a parent,
        // remove it from the original parent's list of children
        if (child._parent !== null) {
            child._parent.remove(child);
        }
        child._parent = this;
    }

    // Called internally by the event manager with events for this panel.
    // or any of its children.
    // Must return true to stop event propagation or false otherwise.
    _onEvent(evn: string, ev: any) :boolean {

        this.dispatch(evn, ev);
        return false;
    }

    // Render draws this panel with rectangular borders on the specified canvas
    _renderRect(ctx: CanvasRenderingContext2D) {

        // If this panel is not visible do not render it and none of its children
        if (!this._visible) {
            return;
        }

        // Updates this panel absolute coordinates relative to its parent
        this._updateBounds();

        // Initializes this panel external absolute coordinates and maximum width and height
        let x = this._xabs;
        let y = this._yabs;

        if (this._parent !== null && this._bounded) {
            ctx.save();
            this._parent.clipContent(ctx);
        }

        // Draw margin rectangles
        if (this._marginColor.a !== 0) {
            ctx.fillStyle = this._marginColor.toString();
            if (this._margin.top !== 0) {
                ctx.fillRect(x, y, this._width, this._margin.top);
            }
            if (this._margin.right !== 0) {
                ctx.fillRect(x+this._width-this._margin.right, y, this._margin.right, this._height);
            }
            if (this._margin.bottom !== 0) {
                ctx.fillRect(x, y+this._height-this._margin.bottom, this._width, this._margin.bottom);
            }
            if (this._margin.left !== 0) {
                ctx.fillRect(x, y, this._margin.left, this._height);
            }
        }
        x += this._margin.left;
        y += this._margin.top;

        // Draw border rectangles
        if (this._borderColor.a !== 0) {
            ctx.fillStyle = this._borderColor.toString();
            const width = this._calcBorderWidth();
            const height = this._calcBorderHeight();
            if (this._border.top !== 0) {
                ctx.fillRect(x, y, width, this._border.top);
            }
            if (this._border.right !== 0) {
                ctx.fillRect(x+width-this._border.right, y, this._border.right, height);
            }
            if (this._border.bottom !== 0) {
                ctx.fillRect(x, y+height-this._border.bottom, width, this._border.bottom);
            }
            if (this._border.left !== 0) {
                ctx.fillRect(x, y, this._border.left, height);
            }
        }
        x += this._border.left;
        y += this._border.top;

        // Draw padding rectangles
        if (this._paddingColor.a !== 0) {
            ctx.fillStyle = this._paddingColor.toString();
            const width = this._calcPaddingWidth();
            const height = this._calcPaddingHeight();
            if (this._padding.top !== 0) {
                ctx.fillRect(x, y, width, this._padding.top);
            }
            if (this._padding.right !== 0) {
                ctx.fillRect(x+width-this._padding.right, y, this._padding.right, height);
            }
            if (this._padding.bottom !== 0) {
                ctx.fillRect(x, y+height-this._padding.bottom, width, this._padding.bottom);
            }
            if (this._padding.left !== 0) {
                ctx.fillRect(x, y, this._padding.left, height);
            }
        }
        x += this._padding.left;
        y += this._padding.top;

        // Draw content
        if (this._contentColor.a !== 0) {
            ctx.fillStyle = this._contentColor.toString();
            ctx.fillRect(x, y, this._content.width, this._content.height);
        }

        if (this._parent !== null && this._bounded) {
            ctx.restore();
        }
        this._changed = false;
    
    }

    // Render draws this panel with rounded borders on the specified canvas
    _renderRounded(ctx: CanvasRenderingContext2D) {
    
        // If this panel is not visible do not render it and none of its children
        if (!this._visible) {
            return;
        }

        // Updates this panel absolute coordinates relative to its parent
        this._updateBounds();

        // Initializes this panel external absolute coordinates
        let x = this._xabs;
        let y = this._yabs;

        // If this panel has a parent and it is bounded, sets its clip region to the
        // the content area of the parent.
        if (this._parent !== null && this._bounded) {
            ctx.save();
            this._parent.clipContent(ctx);
        }

        // Draw margin rectangle if necessary
        if (this._marginColor.a !== 0 && !this._margin.isEmpty()) {
            ctx.fillStyle = this._marginColor.toString();
            ctx.fillRect(x, y, this._width, this._height);
        }

        // Set x, y to origin of the border rectangle
        x += this._margin.left;
        y += this._margin.top;
        const xBorder = x;
        const yBorder = y;

        // Set clip path to the border path and draw paddings
        this._setBorderPath(ctx, xBorder, yBorder);
        ctx.save();
        ctx.clip();
        x += this._border.left;
        y += this._border.top;

        // Draw padding rectangles
        if (this._paddingColor.a !== 0) {
            ctx.fillStyle = this._paddingColor.toString();
            const width = this._calcPaddingWidth();
            const height = this._calcPaddingHeight();
            if (this._padding.top !== 0) {
                ctx.fillRect(x, y, width, this._padding.top);
            }
            if (this._padding.right !== 0) {
                ctx.fillRect(x+width-this._padding.right, y, this._padding.right, height);
            }
            if (this._padding.bottom !== 0) {
                ctx.fillRect(x, y+height-this._padding.bottom, width, this._padding.bottom);
            }
            if (this._padding.left !== 0) {
                ctx.fillRect(x, y, this._padding.left, height);
            }
        }
        // Set x, y to origin of the content rectangle
        x += this._padding.left;
        y += this._padding.top;

        // Draw content
        if (this._contentColor.a !== 0) {
            ctx.fillStyle = this._contentColor.toString();
            ctx.fillRect(x, y, this._content.width, this._content.height);
        }
        ctx.restore();

        // Draw border 
        if (this._border.top > 0) {
            this._setBorderPath(ctx, xBorder, yBorder);
            ctx.strokeStyle = this._borderColor.toString();
            ctx.stroke();
        }

        // Remove parent clip region if necessary
        if (this._parent !== null && this._bounded) {
            ctx.restore();
        }
        this._changed = false;
    }

    // Sets the rounded border path
    _setBorderPath(ctx: CanvasRenderingContext2D, x: number, y: number) {

        const width = this._calcBorderWidth();
        const height = this._calcBorderHeight();
        ctx.lineWidth = this._border.top;
        ctx.beginPath();
        const rx = x + ctx.lineWidth/2;
        const ry = y + ctx.lineWidth/2;
        const rwidth = width - ctx.lineWidth;
        const rheight = height - ctx.lineWidth;
        ctx.moveTo(rx + this._borderRadius, ry);
        ctx.arcTo(rx + rwidth, ry, rx + rwidth, ry + rheight, this._borderRadius);
        ctx.arcTo(rx + rwidth, ry + rheight, rx, ry + rheight, this._borderRadius);
        ctx.arcTo(rx, ry + rheight, rx, ry, this._borderRadius);
        ctx.arcTo(rx, ry, rx + this._borderRadius, ry, this._borderRadius);
    }

    // updateBounds calculates the absolute coordinates of this panel.
    // It assumes that updateBounds() was already called for this panel's parent.
    _updateBounds() {

        if (this._parent === null) {
            this._xabs = this._x;
            this._yabs = this._y;
            this._xabsMin = -100000;
            this._xabsMax = 100000;
            this._yabsMin = -100000;
            this._yabsMax = 100000;

        } else {

            // Sets the absolute position coordinates for this panel
            const pp = this._parent;
            this._xabs = this._x + pp._xabs;
            this._yabs = this._y + pp._yabs;
            if (this._bounded) {
                this._xabs = this._x + pp._xabs + pp._margin.left + pp._border.left + pp._padding.left;
                this._yabs = this._y + pp._yabs + pp._margin.top + pp._border.top + pp._padding.top;
            }

            // Initialize maximum and minimum x,y coordinates for this panel
            this._xabsMin = this._xabs;
            this._yabsMin = this._yabs;
            this._xabsMax = this._xabs + this._width;
            this._yabsMax = this._yabs + this._height;

            if (this._bounded) {
                // Calculates the parent content area minimum and maximum absolute coordinates in pixels
                let pxmin = pp._xabs + pp._margin.left + pp._border.left + pp._padding.left;
                if (pxmin < pp._xabsMin) {
                    pxmin = pp._xabsMin;
                }
                let pymin = pp._yabs + pp._margin.top + pp._border.top + pp._padding.top;
                if (pymin < pp._yabsMin) {
                    pymin = pp._yabsMin;
                }

                let pxmax = pp._xabs + pp._width - (pp._margin.right + pp._border.right + pp._padding.right);
                if (pxmax > pp._xabsMax) {
                    pxmax = pp._xabsMax;
                }
                let pymax = pp._yabs + pp._height - (pp._margin.bottom + pp._border.bottom + pp._padding.bottom);
                if (pymax > pp._yabsMax) {
                    pymax = pp._yabsMax;
                }

                // Update this panel minimum x and y coordinates.
                if (this._xabsMin < pxmin) {
                    this._xabsMin = pxmin;
                }
                if (this._yabsMin < pymin) {
                    this._yabsMin = pymin;
                }
                // Update this panel maximum x and y coordinates.
                if (this._xabsMax > pxmax) {
                    this._xabsMax = pxmax;
                }
                if (this._yabsMax > pymax) {
                    this._yabsMax = pymax;
                }
            }
        }
    }
}
