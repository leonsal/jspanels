import * as font        from "../core/font";
import * as color       from "../core/color";
import * as rect        from "../core/rect";
import * as util        from "../util/clone";
import * as icon        from "../assets/icons";
import * as consts      from "../widgets/consts";
import * as label       from "../widgets/label_style";
import * as button      from "../widgets/button_styles";
import * as splitter    from "../widgets/splitter_styles";
import * as window      from "../widgets/window_styles";
import * as tuner       from "../widgets/tuner_styles";
import * as theme       from "./type"

export function Light():theme.Theme {

    const g: theme.Global = {
        textFont:           font.New(14, font.UnitPx, font.FamilySansSerif),
        iconFont:           font.New(14, font.UnitPx, "Material Icons"),
        fgColor:            color.Name("black"),
        bgColor:            color.Name("lightgray"),
        borderColor:        color.Name("gray"),
        borderRound:        [4, 1],
        fgColorDisabled:    color.Name("gray"),
    }

    const labelStyle: label.Style = {
        border:         new rect.Border(0, 0, 0, 0),
        padding:        new rect.Border(2, 1, 0, 1),
        borderColor:    color.RGBA(0,0,0,0),
        bgColor:        color.RGBA(0,0,0,0),     // transparent
        fgColor:        g.fgColor,
        font:           g.textFont,
    };

    // Button
    let buttonNormal: button.Style = {
        margin:         new rect.Border(),
        border:         new rect.Border(1, 1, 1, 1),
        padding:        new rect.Border(2, 1, 1, 1),
        borderRound:    g.borderRound,
        borderColor:    g.borderColor,
        bgColor:        g.bgColor,
        fgColor:        g.fgColor,
    };
    let buttonOver: button.Style = util.clone(buttonNormal);
    buttonOver.bgColor.mult(1.05);
    buttonOver.fgColor = color.Name("blue");
    let buttonFocus: button.Style = util.clone(buttonOver);
    let buttonPressed: button.Style = util.clone(buttonNormal);
    let buttonDisabled: button.Style = util.clone(buttonNormal);
    const buttonStyles: button.Styles = {
        textFont:   g.textFont,
        iconFont:   g.iconFont,
        iconPos:    consts.IconLeft,
        iconOn:     "",
        iconOff:    "",
        normal:     buttonNormal,
        over:       buttonOver,
        focus:      buttonFocus,
        pressed:    buttonPressed,
        disabled:   buttonDisabled,
    };

    // Checkbox
    let checkboxNormal: button.Style = {
        margin:         new rect.Border(),
        border:         new rect.Border(0, 0, 0, 0),
        padding:        new rect.Border(2, 1, 1, 1),
        borderColor:    g.borderColor,
        borderRound:    null,
        bgColor:        color.RGBA(0,0,0,0),
        fgColor:        g.fgColor,
    }
    let checkboxOver = util.clone(checkboxNormal);
    checkboxOver.bgColor.mult(1.05);
    let checkboxFocus = util.clone(checkboxOver);
    let checkboxPressed = util.clone(checkboxOver);
    checkboxPressed.bgColor.mult(1.1);
    let checkboxDisabled = util.clone(checkboxNormal);
    const checkboxStyles: button.Styles  = {
        textFont:       g.textFont,
        iconFont:       g.iconFont,
        iconPos:        consts.IconLeft,
        iconOn:         icon.CheckBox,
        iconOff:        icon.CheckBoxOutlineBlank,
        normal:         checkboxNormal,
        over:           checkboxOver,
        focus:          checkboxFocus,
        pressed:        checkboxPressed,
        disabled:       checkboxDisabled,
    };

    // Radio
    let radioNormal: button.Style = {
        margin:         new rect.Border(),
        border:         new rect.Border(0, 0, 0, 0),
        padding:        new rect.Border(2, 1, 1, 1),
        borderColor:    g.borderColor,
        borderRound:    null,
        bgColor:        color.RGBA(0,0,0,0),
        fgColor:        g.fgColor,
    };
    let radioOver = util.clone(radioNormal);
    radioOver.bgColor.mult(1.05);
    radioOver.fgColor = color.Name("blue");
    let radioFocus = util.clone(radioOver);
    let radioPressed = util.clone(radioNormal);
    radioPressed.bgColor.mult(1.1);
    let radioDisabled = util.clone(radioNormal);
    const radioStyles: button.Styles = {
        textFont:       g.textFont,
        iconFont:       g.iconFont,
        iconPos:        consts.IconLeft,
        iconOn:         icon.RadioButtonChecked,
        iconOff:        icon.RadioButtonUnchecked,
        normal:         radioNormal,
        over:           radioOver,
        focus:          radioFocus,
        pressed:        radioPressed,
        disabled:       radioDisabled
    };

    // Splitter
    let splitterNormal: splitter.Style = {
        spacerCursorH:      "default",
        spacerCursorV:      "default",
        spacerBorderColor:  g.borderColor,
        spacerColor:        g.bgColor,
        spacerSize:         4,
    }
    let splitterOver = util.clone(splitterNormal);
    splitterOver.spacerCursorH = "ew-resize";
    splitterOver.spacerCursorV = "ns-resize";
    let splitterDrag = util.clone(splitterNormal);
    splitterDrag.spacerCursorH = "ew-resize";
    splitterDrag.spacerCursorV = "ns-resize";
    const splitterStyles: splitter.Styles = {
        normal: splitterNormal,
        over:   splitterOver,
        drag:   splitterDrag,
    }

    // Window
    let windowNormal: window.Style = {
        margin:         new rect.Border(),
        border:         new rect.Border(2, 2, 2, 2),
        padding:        new rect.Border(),
        borderRound:    null,
        borderColor:    g.borderColor,
        bgColor:        g.bgColor,
        header: {
            margin:         new rect.Border(),
            border:         new rect.Border(),
            padding:        new rect.Border(),
            borderRound:    g.borderRound,
            borderColor:    g.borderColor,
            bgColor:        g.bgColor,
            fgColor:        g.fgColor,
        }
    };
    let windowOver: window.Style = util.clone(windowNormal);
    windowOver.bgColor.mult(1.05);
    windowOver.header.fgColor = color.Name("blue");
    let windowFocus: window.Style = util.clone(windowOver);
    let windowPressed: window.Style = util.clone(windowNormal);
    let windowDisabled: window.Style = util.clone(windowNormal);
    const windowStyles: window.Styles = {
        normal:     windowNormal,
        over:       windowOver,
        focus:      windowFocus,
        disabled:   windowDisabled,
    };

    // Tuner
    let tunerNormal: tuner.Style = {
        margin:         new rect.Border(0),
        border:         new rect.Border(1, 1, 1, 1),
        padding:        new rect.Border(1, 1, 0, 1),
        borderRound:    g.borderRound,
        borderColor:    g.borderColor,
        bgColor:        g.bgColor,
        fgColor:        g.fgColor,
        digit: {
            leftZeros: {
                bgColor: g.bgColor,
                fgColor: color.Name("gray"),
            },
            bgColor:    g.bgColor,
            fgColor:    g.fgColor,
        }
    };
    let tunerDisabled = util.clone(tunerNormal);
    tunerDisabled.fgColor = g.fgColorDisabled;
    let tunerOver = util.clone(tunerNormal);
    tunerOver.digit.bgColor = color.Name("white");
    let tunerFocus = util.clone(tunerOver);
    const tunerStyles: tuner.Styles = {
        textFont:       font.New(48, font.UnitPx, font.FamilySansSerif),
        normal:         tunerNormal,
        over:           tunerOver,
        focus:          tunerFocus,
        disabled:       tunerDisabled,
    };

    // Builds and returns theme object
    const t: theme.Theme = {
        g:         g,
        label:     labelStyle,
        button:    buttonStyles,
        checkbox:  checkboxStyles,
        radio:     radioStyles,
        splitter:  splitterStyles,
        window:    windowStyles,
        tuner:     tunerStyles,
    }
    return t;
}


