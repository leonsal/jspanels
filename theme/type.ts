import * as font        from "../core/font";
import * as color       from "../core/color";
import * as rect        from "../core/rect";
import * as util        from "../util/clone";
import * as icon        from "../assets/icons";
import * as consts      from "../widgets/consts";
import * as label       from "../widgets/label_style";
import * as button      from "../widgets/button_styles";
import * as splitter    from "../widgets/splitter_styles";
import * as tuner       from "../widgets/tuner_styles";
import * as window      from "../widgets/window_styles";

export type Global = {
    textFont:           font.Font,
    iconFont:           font.Font,
    fgColor:            color.Color,
    bgColor:            color.Color,
    borderColor:        color.Color,
    borderRound:        number[];
    fgColorDisabled:    color.Color,
}

export type Theme = {
    g:              Global;
    label:          label.Style;
    button:         button.Styles;
    checkbox:       button.Styles;
    radio:          button.Styles;
    splitter:       splitter.Styles;
    window:         window.Styles;
    tuner:          tuner.Styles;
}
