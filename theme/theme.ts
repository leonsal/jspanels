import * as theme from "./type";
import * as light from "./light";
import * as util from "../util/clone";
import { Style as labelStyle } from "../widgets/label_style";
import { Styles as buttonStyles } from "../widgets/button_styles";
import { Styles as splitterStyles } from "../widgets/splitter_styles";
import { Styles as tunerStyles } from "../widgets/tuner_styles";
import { Styles as windowStyles } from "../widgets/window_styles";

// Current theme object
var current: theme.Theme;

/** Returns the current theme object */
export function get(): theme.Theme {

    if (!current) {
        current = light.Light();
    }
    return current;
}

/** Returns a copy of the label style for the current theme */
export function label(): labelStyle {

    return util.clone(get().label);
}

/** Returns a copy of the button style for the current theme */
export function button(): buttonStyles {

    return util.clone(get().button);
}

/** Returns a copy of the checkbox style for the current theme */
export function checkbox(): buttonStyles {

    return util.clone(get().checkbox);
}

/** Returns a copy of the radio style for the current theme */
export function radio(): buttonStyles {

    return util.clone(get().radio);
}

/** Returns a copy of the splitter styles for the current theme */
export function splitter(): splitterStyles {

    return util.clone(get().splitter);
}

/** Returns a copy of the window styles for the current theme */
export function window(): windowStyles {

    return util.clone(get().window);
}

// Returns a copy of the tuner style for the current theme
export function tuner(): tunerStyles {

    return util.clone(get().tuner);
}
