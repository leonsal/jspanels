import * as tests from "./tests";
import {Panel} from "./core/panel";
import * as color from "./core/color";
import * as renderer from "./core/renderer";

export function run() {

    let canvas = document.createElement("canvas");
    canvas.width = 1200;
    canvas.height = 800;
    let body = document.getElementsByTagName("body");
    body[0].appendChild(canvas);

    let ctx = canvas.getContext("2d");
    if (ctx == null) {
        return;
    }
    ctx.fillStyle = "lightgray";
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    const root = newRoot();
    const rend = renderer.New(canvas, root);
    rend.start();
}
tests.All.set("panel", run);

function newRoot() {

    //const root = panel.New(0, 0);
    //root.setPos(0, 0);
    //root.setMargin(10, 10, 10, 10);
    //root.setMarginColor(color.New("yellow"));
    //root.setBorder(10, 10, 10, 10);
    //root.setBorderRound(30, 4);
    //root.setBorderColor(color.New("black"));
    //root.setPadding(10, 10, 10, 10);
    //root.setBgColor(color.New("red"));
    //root.setPaddingColor(color.New("green"));
    //root.setSize(700, 350);



    const root = new Panel(0, 0);
    root.setPos(40, 40);
    root.setMargin(10, 10, 10, 10);
    root.setMarginColor(color.Name("yellow"));
    root.setBorder(10, 10, 10, 10);
    root.setBorderColor(color.Name("black"));
    root.setPadding(10, 10, 10, 10);
    //root.SetBgColor(core.NewColor("green"))
    root.setPaddingColor(color.Name("white"));
    root.setSize(700, 350);

    const cTL = newChild(true);
    cTL.setPos(-20, -20);
    root.add(cTL);

    const cTR = newChild(true);
    cTR.setPos(550, -20);
    root.add(cTR);

    const cBL = newChild(true);
    cBL.setPos(-20, 200);
    root.add(cBL);

    const cBR = newChild(true);
    cBR.setPos(550, 200);
    root.add(cBR);

    const cML = newChild(true);
    cML.setPos(150, 100);
    root.add(cML);

    const cMR = newChild(false);
    cMR.setPos(370, 100);
    root.add(cMR);

    return root;
}


function newChild(bounded: boolean) {

    const p = new Panel(100, 100);
    p.setMargin(2, 2, 2, 2);
    p.setMarginColor(color.Name("yellow"));
    p.setBorder(2, 2, 2, 2);
    p.setBorderRound(20, 2);
    p.setBorderColor(color.Name("black"));
    p.setBgColor(color.Name("blue"));
    p.setPadding(2, 2, 2, 2);
    p.setPaddingColor(color.Name("green"));
    p.setPos(-20, -20);

    const pTL = new Panel(100, 100);
    pTL.setMargin(2, 2, 2, 2);
    pTL.setMarginColor(color.Name("black"));
    pTL.setBorder(2, 2, 2, 2);
    pTL.setBorderColor(color.Name("red"));
    pTL.setBgColor(color.Name("orange"));
    pTL.setPadding(2, 2, 2, 2);
    pTL.setPaddingColor(color.Name("blue"));
    pTL.setPos(-70, -70);
    pTL.setBounded(bounded);
    p.add(pTL);

    const pTR = new Panel(100, 100);
    pTR.setMargin(2, 2, 2, 2);
    pTR.setMarginColor(color.Name("black"));
    pTR.setBorder(2, 2, 2, 2);
    pTR.setBorderColor(color.Name("red"));
    pTR.setBgColor(color.Name("orange"));
    pTR.setPadding(2, 2, 2, 2);
    pTR.setPaddingColor(color.Name("blue"));
    pTR.setPos(60, -70);
    pTR.setBounded(bounded);
    p.add(pTR);

    const pBL = new Panel(100, 100);
    pBL.setMargin(2, 2, 2, 2);
    pBL.setMarginColor(color.Name("black"));
    pBL.setBorder(2, 2, 2, 2);
    pBL.setBorderColor(color.Name("red"));
    pBL.setBgColor(color.Name("orange"));
    pBL.setPadding(2, 2, 2, 2);
    pBL.setPaddingColor(color.Name("blue"));
    pBL.setPos(-70, 60);
    pBL.setBounded(bounded);
    p.add(pBL);

    const pBR = new Panel(100, 100);
    pBR.setMargin(2, 2, 2, 2);
    pBR.setMarginColor(color.Name("black"));
    pBR.setBorder(2, 2, 2, 2);
    pBR.setBorderColor(color.Name("red"));
    pBR.setBgColor(color.Name("orange"));
    pBR.setPadding(2, 2, 2, 2);
    pBR.setPaddingColor(color.Name("blue"));
    pBR.setPos(60, 60);
    pBR.setBounded(bounded);
    p.add(pBR);

    return p;
}


