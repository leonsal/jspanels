import * as tests from "./tests";
import * as color from "./core/color";
import {Panel} from "./core/panel";
import * as renderer from "./core/renderer";
import * as evm from "./core/event_manager";
import * as event from "./core/event";
import * as button from "./widgets/button";
import * as radio from "./widgets/radio";
import * as checkbox from "./widgets/checkbox";
import * as hbox from "./layout/hbox";

export function run() {

    let canvas = document.createElement("canvas");
    canvas.width = 800;
    canvas.height = 800;
    let body = document.getElementsByTagName("body");
    body[0].appendChild(canvas);

    let ctx = canvas.getContext("2d");
    if (ctx === null) {
        throw new Error("getContext('2d')")
    }

    const root = new Panel(0, 0);
    root.setBgColor(color.Name("blue"));
    root.setPos(10, 10);
    root.setBorderColor(color.Name("black"));
    root.setBorder(1, 1, 1, 1);
    root.setBgColor(color.Name("whitesmoke"));
    root.setPadding(10, 10, 10, 10);
    root.setSize(canvas.width, canvas.height);

    const pl = new Panel(600, 400);
    pl.setBorderColor(color.Name("black"));
    pl.setBgColor(color.Name("white"));
    pl.setBorder(1, 1, 1, 1);
    const lt = new hbox.HBox(pl);
    pl.setLayout(lt);
    root.add(pl);

    const b1 = button.New("Add child");
    b1.setPos(0, 0);
    b1.update();
    b1.subscribe(event.Click, (/*ev*/) => {
        // Create button
        const text = "Child" + pl.children().length.toString();
        const b = button.New(text);
        let height = b.height();
        b.setHeight(Math.floor(height + 100 * Math.random()));
        // Sets the button layout parameters
        const params = <hbox.Params>hbox.HBox.childParams();
        if (rtop.value()) {
            params.align = hbox.AlignChild.Top;
        } else 
        if (rcenter.value()) {
            params.align = hbox.AlignChild.Center;
        } else 
        if (rbottom.value()) {
            params.align = hbox.AlignChild.Bottom;
        } else
        if (rheight.value()) {
            params.align = hbox.AlignChild.Height;
        }
        if (cexpand.value()) {
            params.expand = 1;
        }
        b.setLayoutParams(params);
        console.log("params:", b.layoutParams());
        pl.add(b);
    });
    root.add(b1);

    const groupV = "alignV";
    const rtop = radio.New(hbox.AlignChild.Top, groupV);
    rtop.setValue(true);
    rtop.setPos(b1.posX() + b1.width() + 5, b1.posY());
    rtop.update();
    root.add(rtop);

    const rcenter = radio.New(hbox.AlignChild.Center, groupV);
    rcenter.setPos(rtop.posX() + rtop.width() + 5, rtop.posY());
    rcenter.update();
    root.add(rcenter);

    const rbottom = radio.New(hbox.AlignChild.Bottom, groupV);
    rbottom.setPos(rcenter.posX() + rcenter.width() + 5, rcenter.posY());
    rbottom.update();
    root.add(rbottom);

    const rheight = radio.New(hbox.AlignChild.Height, groupV);
    rheight.setPos(rbottom.posX() + rbottom.width() + 5, rbottom.posY());
    rheight.update();
    root.add(rheight);

    const cexpand = checkbox.New("Expand");
    cexpand.setPos(rheight.posX() + rheight.width() + 5, rheight.posY());
    cexpand.update();
    root.add(cexpand);

    const bRem = button.New("Remove");
    bRem.setPos(cexpand.posX() + rheight.width() + 20, 0);
    bRem.update();
    root.add(bRem);
    bRem.subscribe(event.Click, () => {
        const nchildren = pl.children().length;
        if (nchildren == 0) {
            return;
        }
        const child = pl.children()[nchildren-1];
        pl.remove(child);
    });

    const setAlignH = (align: string) => {
        lt.setAlign(<hbox.AlignGroup>align);
    };

    const groupH = "alignH";
    const rleft = radio.New(hbox.AlignGroup.Left, groupH);
    rleft.setValue(true);
    rleft.setPos(b1.posX(), b1.posY() + b1.height() + 5);
    rleft.update();
    rleft.subscribe(event.Click, ()=>setAlignH(rleft.text()));
    root.add(rleft);

    const rcenterh = radio.New(hbox.AlignGroup.Center, groupH);
    rcenterh.setPos(rleft.posX() + rleft.width() + 5, rleft.posY());
    rcenterh.update();
    rcenterh.subscribe(event.Click, ()=>setAlignH(rcenter.text()));
    root.add(rcenterh);

    const rright = radio.New(hbox.AlignGroup.Right, groupH);
    rright.setPos(rcenterh.posX() + rcenterh.width() + 5, rcenterh.posY());
    rright.update();
    rright.subscribe(event.Click, ()=>setAlignH(rright.text()));
    root.add(rright);

    const rwidth = radio.New(hbox.AlignGroup.Width, groupH);
    rwidth.setPos(rright.posX() + rright.width() + 5, rright.posY());
    rwidth.update();
    rwidth.subscribe(event.Click, ()=>setAlignH(rwidth.text()));
    root.add(rwidth);

    const cautoHeight = checkbox.New("AutoHeight");
    cautoHeight.setPos(rwidth.posX() + rwidth.width() + 10, rwidth.posY());
    cautoHeight.update();
    cautoHeight.subscribe(event.Click, ()=>lt.setAutoHeight(cautoHeight.value()));
    root.add(cautoHeight);

    const cautoWidth = checkbox.New("AutoWidth");
    cautoWidth.setPos(cautoHeight.posX() + cautoHeight.width() + 5, cautoHeight.posY());
    cautoWidth.update();
    cautoWidth.subscribe(event.Click, ()=>lt.setAutoWidth(cautoWidth.value()));
    root.add(cautoWidth);

    pl.setPos(0, cautoWidth.posY() + cautoWidth.height() + 5);

    // Starts rendering
    const rend = renderer.New(canvas, root);
    rend.start();

    // Starts event manager
    evm.get().add(canvas, root);
    evm.get().start();
}

tests.All.set("hbox_layout", run);