import * as panel from "../core/panel"
import * as layout from "./layout"

// Group horizontal alignment
export const enum AlignGroup {
    Left    = "Left",
    Right   = "Right",
    Center  = "Center",
    Width   = "Width",
}

// Child vertical alignment
export const enum AlignChild {
    Top     = "Top",
    Bottom  = "Bottom",
    Center  = "Center",
    None    = "None",
    Height  = "Height",
}

// Type for this layout child parameters
export type Params = {
    readonly type: string;     // layout parameters type
    expand: number;            // expand factor (0 - no expand)
    align: AlignChild;         // child vertical alignment
}

export class HBox {

    protected _pan: panel.Panel;
    protected _spacing: number;
    protected _align: AlignGroup;
    protected _autoHeight: boolean;
    protected _autoWidth: boolean;

    constructor(pan: panel.Panel) {

        this._pan = pan;                // target panel
        this._spacing = 0;              // spacing between children
        this._align = AlignGroup.Left;  // horizontal alignment of the children
        this._autoHeight = false;       // auto height state
        this._autoWidth = false;        // auto width state
    }

    static childParams(): layout.IParams {

        return Object.seal({ type: "hbox", expand: 0, align: AlignChild.Top });
    }

    // Returns the default layout parameters for children of the panel with this layout.
    childParams(): layout.IParams {

        return HBox.childParams();
    }

    // Sets the horizontal spacing between the items in pixels and updates the layout.
    setSpacing(spacing: number) {

        this._spacing = spacing;
        this.update();
    }

    // Sets the horizontal alignment of the whole group of items
    // inside the parent panel and updates the layout.
    // This only has any effect if there are no expanded items.
    setAlign(align: AlignGroup) {

        this._align = align;
        this.update();
    }

    // Set if the panel minimum height should be the height of
    // the largest of its children's height.
    setAutoHeight(state: boolean) {

        this._autoHeight = state;
        this.update();
    }

    // Sets if the panel minimum width should be sum of its
    // children's width plus the spacing
    setAutoWidth(state: boolean) {

        this._autoWidth = state;
        this.update();
    }

    // Updates the layout
    update() {

        // Fast path for panel with no children
        const nchildren = this._pan.children().length;
        if (nchildren === 0) {
            return;
        }

        // Updates all children dimensions
        for (const child of this._pan.children()) {
            child.update();
        }

        // If autoHeight is set, get the maximum height of all the panel's children
        // and if the panel content height is less than this maximum, set its content height to this value.
        if (this._autoHeight) {
            let maxHeight = 0;
            for (const child of this._pan.children()) {
                if (!child.visible()) {
                    continue;
                }
                if (child.height() > maxHeight) {
                    maxHeight = child.height();
                }
            }
            if (this._pan.contentHeight() < maxHeight) {
                this._pan._setContentSize(this._pan.contentWidth(), maxHeight, false);
            }
        }

        // If autoWidth is set, get the sum of widths of this panel's children plus the spacings.
        // If the panel content width is less than this width, set its content width to this value.
        if (this._autoWidth) {
            let totalWidth = 0;
            for (const child of this._pan.children()) {
                if (!child.visible()) {
                    continue;
                }
                totalWidth += child.width();
            }
            // Adds spacing
            totalWidth += this._spacing * (nchildren - 1);
            if (this._pan.contentWidth() < totalWidth) {
                this._pan._setContentSize(totalWidth, this._pan.contentHeight(), false);
            }
        }

        // Calculates the total width, expanded width, fixed width and
        // the sum of the expand factor for all items.
        let twidth = 0;
        let fwidth = 0;
        let texpand = 0;
        let ecount = 0;
        for (let pos = 0; pos < nchildren; pos++) {
            const child = this._pan.children()[pos];
            if (!child.visible()) {
                continue;
            }
            // Get child layout parameters
            const params = this._params(child);
            // Calculate total width
            twidth += child.width();
            if (pos > 0) {
                twidth += this._spacing;
            }
            // Calculate width of expanded items
            if (params.expand > 0) {
                texpand += params.expand;
                ecount++;
                // Calculate width of fixed items
            } else {
                fwidth += child.width();
                if (pos > 0) {
                    fwidth += this._spacing;
                }
            }
        }

        // If there is at least on expanded item, all free space will be occupied
        let spaceMiddle = this._spacing;
        let posX = 0;
        if (texpand > 0) {
            // If there is free space, distribute space between expanded items
            let totalSpace = this._pan.contentWidth() - twidth;
            if (totalSpace > 0) {
                for (const child of this._pan.children()) {
                    if (!child.visible()) {
                        continue;
                    }
                    const params = this._params(child);
                    if (params.expand > 0) {
                        let iwidth = totalSpace * params.expand / texpand;
                        child.setWidth(child.width() + iwidth);
                    }
                }
                // No free space: distribute expanded items widths
            } else {
                for (const child of this._pan.children()) {
                    if (!child.visible()) {
                        continue;
                    }
                    const params = this._params(child);
                    if (params.expand > 0) {
                        const spacing = this._spacing * (ecount - 1);
                        const iwidth = (this._pan.contentWidth() - spacing - fwidth - this._spacing) * params.expand / texpand;
                        child.setWidth(iwidth);
                    }
                }
            }
            // No expanded items: checks block horizontal alignment
        } else {
            // Calculates initial x position which depends
            // on the current horizontal alignment.
            switch (this._align) {
                case AlignGroup.Left:
                    posX = 0;
                    break;
                case AlignGroup.Center:
                    posX = (this._pan.contentWidth() - twidth) / 2;
                    break;
                case AlignGroup.Right:
                    posX = this._pan.contentWidth() - twidth;
                    break;
                case AlignGroup.Width: {
                    let space = this._pan.contentWidth() - twidth + this._spacing * (nchildren - 1);
                    if (space < 0) {
                        space = this._spacing * (nchildren - 1);
                    }
                    spaceMiddle = space / (nchildren + 1);
                    posX = spaceMiddle;
                }
                    break;
                default:
                    throw new Error("HBox layout with invalid global horizontal alignment");
            }
        }

        // Calculates the Y position of each item considering its vertical alignment
        let posY = 0;
        const height = this._pan.contentHeight();
        for (let pos = 0; pos < nchildren; pos++) {
            const child = this._pan.children()[pos];
            if (!child.visible()) {
                continue;
            }
            const params = this._params(child);
            const cheight = child.height();
            switch (params.align) {
                case AlignChild.None:
                case AlignChild.Top:
                    posY = 0;
                    break;
                case AlignChild.Center:
                    posY = (height - cheight) / 2;
                    break;
                case AlignChild.Bottom:
                    posY = height - cheight;
                    break;
                case AlignChild.Height:
                    posY = 0;
                    child.setHeight(height);
                    break;
                default:
                    throw new Error("HBox layout with invalid child vertical alignment");
            }
            // Sets the child position
            child.setPos(posX, posY);
            // Calculates next child position
            posX += child.width();
            if (pos < nchildren - 1) {
                posX += spaceMiddle;
            }
        }
    }

    _params(child: panel.Panel): Params {

        // Get layout parameters from child and checks if its valid for this layout
        let iparams = child.layoutParams();
        let params: Params;
        if (iparams !== null) {
            if (iparams.type != "hbox") {
                throw new Error("Layout child with invalid child parameters");
            } else {
                params = <Params>iparams;
            }
        } else {
            params = <Params>HBox.childParams();
        }
        return params;
    }
}

