import * as panel from "../core/panel"
import * as layout from "./layout"

/** Group vertical alignment */
export const enum AlignGroup {
    Top    = "Top",
    Center = "Center",
    Bottom = "Bottom",
    Height = "Height",
    None   = "None",
}

/** Child horizontal alignment */
export const enum AlignChild {
    Left = "Left",
    Center = "Center",
    Right = "Right",
    None = "None",
    Width = "Width",
}

/** Type for this layout child parameters */
export type Params = {
    readonly type: string;      // layout parameters type
    expand: number,             // item expand vertically factor (0 - no expand)
    align: AlignChild,          // child horizontal alignment
}

export class VBox {

    protected _pan: panel.Panel;
    protected _spacing: number;
    protected _align: AlignGroup;
    protected _autoHeight: boolean;
    protected _autoWidth: boolean;

    constructor(pan: panel.Panel) {

        this._pan = pan;                // target panel
        this._spacing = 0;              // spacing between children
        this._align = AlignGroup.Top;   // vertical alignment of the children group
        this._autoHeight = false;       // auto height state
        this._autoWidth = false;        // auto width state
    }
    static childParams(): layout.IParams {

        return Object.seal({ type: "vbox", expand: 0, align: AlignChild.Left });
    }

    // Returns the default layout parameters for children of the panel with this layout.
    childParams(): layout.IParams {

        return VBox.childParams();
    }

    /** 
     * Sets the vertical spacing between the items in pixels and updates the layout.
     * @param spacing betwee items in pixels
     */
    setSpacing(spacing: number) {

        this._spacing = spacing;
        this.update();
    }

    /**
     * Sets the vertical alignment of the whole group of items
     * inside the parent panel and updates the layout.
     * This only has any effece if there are no expande items.
     * @param vertical alignment
     */ 
    setAlign(align: AlignGroup) {

        this._align = align;
        this.update();
    }

    // Set if the panel minimum height should be the height of
    // the largest of its children's height.
    setAutoHeight(state: boolean) {

        this._autoHeight = state;
        this.update();
    }

    // Sets if the panel minimum width should be sum of its
    // children's width plus the spacing
    setAutoWidth(state: boolean) {

        this._autoWidth = state;
        this.update();
    }

    update() {

        // Fast path for panel with no children
        const nchildren = this._pan.children().length;
        if (nchildren === 0) {
            return;
        }

        // Updates all children dimensions
        for (const child of this._pan.children()) {
            child.update();
        }

        // If autoHeight is set, get the sum of heights of this panel's children plus the spacings.
        // If the panel content height is less than this height, set its content height to this value.
        if (this._autoHeight) {

            let totalHeight = 0;
            for (const child of this._pan.children()) {
                if (!child.visible() || !child.bounded()) {
                    continue;
                }
                totalHeight += child.height();
            }
            // Adds spacing
            totalHeight += this._spacing * (nchildren - 1);
            if (this._pan.contentHeight() < totalHeight) {
                this._pan._setContentSize(this._pan.contentWidth(), totalHeight, false);
            }
        }

        // If autoWidth is set, get the maximum width of all the panel's children
        // and if the panel content width is less than this maximum, set its content width to this value.
        if (this._autoWidth) {

            let maxWidth = 0;
            for (const child of this._pan.children()) {
                if (!child.visible() || !child.bounded()) {
                    continue;
                }
                if (child.width() > maxWidth) {
                    maxWidth = child.width();
                }
            }
            if (this._pan.contentWidth() < maxWidth) {
                this._pan._setContentSize(maxWidth, this._pan.contentHeight(), false);
            }
        }

        // Calculates the total height, expanded height, fixed height and
        // the sum of the expand factor for all items.
        let theight = 0;
        let fheight = 0;
        let texpand = 0;
        let ecount = 0;
        for (let pos = 0; pos < nchildren; pos++) {
            const child = this._pan.children()[pos];
            if (!child.visible()) {
                continue;
            }
            // Calculate total height
            const params = this._params(child);
            theight += child.height();
            if (pos > 0) {
                theight += this._spacing;
            }
            // Calculate height of expanded items
            if (params.expand > 0) {
                texpand += params.expand;
                ecount++;
                // Calculate height of fixed items
            } else {
                fheight += child.height();
                if (pos > 0) {
                    fheight += this._spacing;
                }
            }
        }

	    // If there is at least on expanded item, all free space will be occupied
	    let spaceMiddle = this._spacing;
	    let posY = 0;
	    if (texpand > 0) {
	    	// If there is free space, distribute space between expanded items
	    	const totalSpace = this._pan.contentHeight() - theight;
	    	if (totalSpace > 0) {
	    		for (const child of this._pan.children()) {
	    			if (!child.visible() || !child.bounded()) {
	    				continue;
	    			}
                    const params = this._params(child);
	    			if (params.expand > 0) {
	    				const iheight = totalSpace * params.expand / texpand;
	    				child.setHeight(child.height() + iheight);
	    			}
	    		}
	    		// No free space: distribute expanded items heights
	    	} else {
	    		for (const child of this._pan.children()) {
	    			if (!child.visible() || !child.bounded()) {
	    				continue;
	    			}
                    const params = this._params(child);
	    			if (params.expand > 0) {
	    				const spacing = this._spacing * (ecount-1);
	    				const iheight = (this._pan.contentHeight() - spacing - fheight - this._spacing) * params.expand / texpand;
	    				child.setHeight(iheight);
	    			}
	    		}
	    	}
	    	// No expanded items: checks block vertical alignment
	    } else {
	    	// Calculates initial y position which depends
	    	// on the current horizontal alignment.
	    	switch (this._align) {
            case AlignGroup.None:
            case AlignGroup.Top:
                posY = 0;
                break;
	    	case AlignGroup.Center:
                posY = (this._pan.contentHeight() - theight) / 2;
                break;
	    	case AlignGroup.Bottom:
                posY = this._pan.contentHeight() - theight;
                break;
	    	case AlignGroup.Height:
	    		let space = this._pan.contentHeight() - theight + this._spacing * (nchildren - 1);
	    		if (space < 0) {
	    			space = this._spacing * (nchildren - 1);
	    		}
	    		spaceMiddle = space / (nchildren + 1);
                posY = spaceMiddle;
                break;
	    	default:
                throw new Error("VBox layout with invalid group alignment");
	    	}
        }

	    // Calculates the X position of each item considering
	    // it horizontal alignment
        let posX :number;
	    const width = this._pan.contentWidth();
	    for (let pos = 0; pos < nchildren; pos++) {
            const child = this._pan.children()[pos];
	    	if (!child.visible() || !child.bounded) {
	    		continue;
	    	}
            const params = this._params(child);
            const cwidth = child.width();
	    	switch (params.align) {
            case AlignChild.None:
            case AlignChild.Left:
                posX = 0;
                break;
	    	case AlignChild.Center:
                posX = (width - cwidth) / 2;
                break;
	    	case AlignChild.Right:
                posX = width - cwidth;
                break;
	    	case AlignChild.Width:
	    		posX = 0;
                child.setWidth(width);
                break;
	    	default:
	    		throw new Error("HBox Layout with invalid child horizontal alignment");
	    	}
	    	// Sets the child position
	    	child.setPos(posX, posY);
	    	// Calculates next position
	    	posY += child.height();
	    	if (pos < nchildren - 1) {
	    		posY += spaceMiddle;
	    	}
	    }
    }

    _params(child: panel.Panel): Params {

        // Get layout parameters from child and checks if its valid for this layout
        let iparams = child.layoutParams();
        let params: Params;
        if (iparams !== null) {
            if (iparams.type != "vbox") {
                throw new Error("Layout child with invalid child parameters");
            } else {
                params = <Params>iparams;
            }
        } else {
            params = <Params>VBox.childParams();
        }
        return params;
    }
}
