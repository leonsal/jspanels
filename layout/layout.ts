
export interface IParams {
    readonly type:  string
}

export interface ILayout {
    childParams():  IParams;
    update():       void;
}
