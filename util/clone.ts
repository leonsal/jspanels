export function clone(obj: any) {

    if (obj.clone !== undefined && typeof(obj.clone) === "function") {
        return obj.clone();
    }
    const c: any = {};
    for (let prop in obj) {
        if (obj[prop] !== null && typeof(obj[prop]) === "object") {
            c[prop] = clone(obj[prop]);
        }
        else {
            c[prop] = obj[prop];
        }
    }
    return c;
}


export function assign(target:any, src:any) {

    for (let prop in src) {
        if (src[prop] !== null && typeof(src[prop]) === "object") {
            if (src[prop].clone !== undefined && typeof(src[prop].clone) === "function") {
                target[prop] = src[prop].clone();
            }
            else {
                throw new Error("cannot clone source property:" + prop);
            }
        }
        else {
            target[prop] = src[prop];
        }
    }
}

