import * as tests from "./tests";
import * as color from "./core/color";
import {Panel} from "./core/panel";
import * as renderer from "./core/renderer";
import * as evm from "./core/event_manager";
import * as window from "./widgets/window";

export function run() {

    let canvas = document.createElement("canvas");
    canvas.width = 800;
    canvas.height = 800;
    let body = document.getElementsByTagName("body");
    body[0].appendChild(canvas);

    let ctx = canvas.getContext("2d");
    if (ctx === null) {
        throw new Error("getContext('2d')")
    }

    const root = new Panel(0, 0);
    root.setBgColor(color.Name("blue"));
    root.setBorderColor(color.Name("black"));
    root.setBorder(1, 1, 1, 1);
    root.setBgColor(color.Name("whitesmoke"));
    root.setPadding(10, 10, 10, 10);
    root.setSize(canvas.width, canvas.height);

    const w1 = new window.Window(400, 200);
    w1.setPos(10, 10);
    root.add(w1);

    // Starts rendering
    const rend = renderer.New(canvas, root);
    rend.start();

    // Starts event manager
    evm.get().add(canvas, root);
    evm.get().start();
}

tests.All.set("window", run);
