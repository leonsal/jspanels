import * as logger from "./util/logger";
import * as tests from "./tests";
import "./t_canvas";
import "./t_panel";
import "./t_label";
import "./t_button";
import "./t_tuner";
import "./t_hbox_layout";
import "./t_vbox_layout";
import "./t_splitter";
import "./t_window";
import "./t_frame";

function main() {

    // Creates logger
    const log = logger.getLogger("Main");
    const hconsole = logger.createConsoleHandler({
        level: logger.DEBUG,
        useColors: true
    });
    log.addHandler(hconsole);
    log.setLevel("DEBUG");
    log.info("Starting...");

    // Get optional test name from url
    let search = window.location.search;
    if (search.length === 0) {
        buildMenu();
        return;
    }

    // Get test module from test name
    let name = search.substring(3);
    let test = tests.All.get(name);
    if (!test) {
        log.info("Invalid test name:%s", name);
        return;
    }
    test();
}

function buildMenu() {

    let body = document.getElementsByTagName("body");
    for (const name of tests.All.keys()) {
        let link = document.createElement("a");
        link.href = "?t=" + name;
        link.style.display = "block";
        link.innerHTML = name;
        body[0].appendChild(link);
    }
}

main();

