import * as tests from "./tests";
import * as color from "./core/color";
import {Panel} from "./core/panel";
import * as renderer from "./core/renderer";
import * as evm from "./core/event_manager";
import {Button} from "./widgets/button";
import {Frame} from "./core/frame";

export function run() {

    let body = document.getElementsByTagName("body");
    const f = new Frame(800, 800);
    body[0].appendChild(f.htmlElement());

    const edit1 = document.createElement("input");
    edit1.style.position = "absolute";
    edit1.setAttribute("type", "text");
    f.div().appendChild(edit1);

    const edit2 = document.createElement("input");
    edit2.style.position = "absolute";
    edit2.setAttribute("type", "text");
    edit2.style.top = "100px";
    f.div().appendChild(edit2);

    const b1 = new Button("Button 1");
    b1.setPos(200, 0);
    f.root().add(b1);

    const b2 = document.createElement("button");
    b2.style.position = "absolute";
    b2.innerHTML = "html button";
    b2.style.left = "400px";
    b2.style.top = "100px";
    f.div().appendChild(b2);

    const text1 = document.createElement("div");
    text1.style.position = "absolute";
    text1.innerHTML = "html <b>text</b> <i>control</i>";
    text1.style.left = "600px";
    text1.style.top = "100px";
    f.div().appendChild(text1);



    f.start();
}
tests.All.set("frame", run);

