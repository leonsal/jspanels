import * as tests from "./tests";

export function run() {

    console.log("test canvas run");
    let canvas = document.createElement("canvas");
    canvas.width = 1200;
    canvas.height = 800;
    let body = document.getElementsByTagName("body");
    body[0].appendChild(canvas);

    let ctx = canvas.getContext("2d");
    if (ctx == null) {
        return;
    }
    ctx.fillStyle = "lightgray";
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    //drawText(ctx);
    drawRoundRects(ctx);
}
tests.All.set("canvas", run);

function drawText(ctx: CanvasRenderingContext2D) {

    let fsize = 10;
    let x = 10;
    let y = 10;
    ctx.fillStyle = "black";
    for (let i = 0; i < 24; i++) {
        ctx.font = fsize.toString() + "px serif";
        ctx.fillText("This is a line with text and numbers: 1234567890", x, y);
        let tm = ctx.measureText("M");
        fsize += 2;
        y += tm.width+4;
    }
}

function drawRoundRects(ctx: CanvasRenderingContext2D) {

    drawGrid(ctx, "gray", 10, 10);

    ctx.beginPath();
    ctx.lineWidth = 2;
    roundRect(ctx, 0 + ctx.lineWidth/2, 0 + ctx.lineWidth/2, 800, 600, 10);
    ctx.strokeStyle = "black";
    ctx.stroke();
}


function roundRect(ctx: CanvasRenderingContext2D,
    x:number, y:number, width:number, height:number, cornerRadius:number) {

    ctx.moveTo(x + cornerRadius, y);
    ctx.arcTo(x + width, y, x + width, y + height, cornerRadius);
    ctx.arcTo(x + width, y + height, x, y + height, cornerRadius);
    ctx.arcTo(x, y + height, x, y, cornerRadius);
    ctx.arcTo(x, y, x + cornerRadius, y, cornerRadius);
}

function drawGrid(ctx: CanvasRenderingContext2D, color: string, stepx:number, stepy:number) {

    ctx.strokeStyle = color;
    ctx.lineWidth = 0.5;
    for (let i = stepx + 0.5; i < ctx.canvas.width; i += stepx) {
        ctx.beginPath();
        ctx.moveTo(i, 0);
        ctx.lineTo(i, ctx.canvas.height);
        ctx.stroke();
    }
    for (let i = stepy + 0.5; i < ctx.canvas.height; i += stepy) {
        ctx.beginPath();
        ctx.moveTo(0, i);
        ctx.lineTo(ctx.canvas.width, i);
        ctx.stroke();
    }
}
