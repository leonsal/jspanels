import * as tests from "./tests";
import {Panel} from "./core/panel";
import * as color from "./core/color";
import * as label from "./widgets/label";
import * as icon  from "./assets/icons";
import * as renderer from "./core/renderer";
import * as rect from "./core/rect";

export function run() {

    const canvas = document.createElement("canvas");
    canvas.width = 1200;
    canvas.height = 800;
    const body = document.getElementsByTagName("body");
    body[0].appendChild(canvas);

    const ctx = canvas.getContext("2d");
    if (ctx == null) {
        throw new Error("getContext()");
    }
    ctx.fillStyle = "lightgray";
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    const root = new Panel(0, 0);
    root.setBgColor(color.Name("blue"));
    root.setPos(50, 50);
    root.setMargin(10, 10, 10, 10);
    root.setBorderColor(color.Name("red"));
    root.setBorder(10, 10, 10, 10);
    root.setBgColor(color.Name("green"));
    root.setPaddingColor(color.Name("white"));
    root.setPadding(10, 10, 10, 10);
    root.setSize(700, 350);

    const label1 = label.New("AbcdefghijklmnopqrstuvwzyZ");
    label1.style().bgColor = color.Name("white");
    label1.style().border = new rect.Border(2, 2, 2, 2);
    label1.style().borderColor = color.Name("red");
    label1.setPos(10, 10);
    root.add(label1);

    const label2 = label.New("Very long label clipped by the parent panel");
    label2.style().border = new rect.Border(1, 1, 1, 1);
    label2.style().borderColor = color.Name("black");
    label2.setPos(402, 50);
    root.add(label2);

    const label3 = label.New(icon.AddCircle + icon.Check + icon.CheckBox);
    label3.style().font.setSize(16);
    label3.style().font.setFamily("Material Icons");
    label3.style().border = new rect.Border(1, 1, 1, 1);
    label3.style().borderColor = color.Name("white");
    label3.setPos(10, 200);
    root.add(label3);

    const label4 = label.New("");
    label4.style().border = new rect.Border(1, 1, 1, 1);
    label4.style().borderColor = color.Name("black");
    label4.style().bgColor = color.Name("white");
    label4.setPos(10, 100);
    root.add(label4);

    let counter = 1;
    let prev = 0;
    const rend = renderer.New(canvas, root);
    rend.subscribe(renderer.OnRender, (evn:string, ts: number) => {
        if (ts - prev > 1000) {
            label4.setText(counter.toString());
            counter++;
            prev = ts;
        }
    });
    rend.start();
}
tests.All.set("label", run);

