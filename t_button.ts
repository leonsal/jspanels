import * as tests from "./tests";
import {Panel} from "./core/panel";
import * as color from "./core/color";
import * as icon  from "./assets/icons";
import * as renderer from "./core/renderer";
import * as event from "./core/event";
import * as evm from "./core/event_manager";
import * as button from "./widgets/button";
import * as checkbox from "./widgets/checkbox";
import * as radio from "./widgets/radio";
import * as consts from "./widgets/consts";

export function run() {

    let body = document.getElementsByTagName("body");

    let canvas1 = document.createElement("canvas");
    canvas1.width = 1200;
    canvas1.height = 400;
    body[0].appendChild(canvas1);
    let ctx1 = canvas1.getContext("2d");
    if (ctx1 == null) {
        throw new Error("getContext('2d')");
    }
    ctx1.fillStyle = "lightgray";
    ctx1.fillRect(0, 0, canvas1.width, canvas1.height);

    let canvas2 = document.createElement("canvas");
    canvas2.width = 800;
    canvas2.height = 400;
    body[0].appendChild(canvas2);
    let ctx2 = canvas2.getContext("2d");
    if (ctx2 == null) {
        throw new Error("getContext('2d')");
    }
    ctx2.fillStyle = "lightgray";
    ctx2.fillRect(0, 0, canvas2.width, canvas2.height);

    const root1 = new Panel(0, 0);
    root1.setBorder(1,1,1,1);
    root1.setBgColor(color.Name("white"));
    root1.setBorderColor(color.Name("black"));
    root1.setSize(canvas1.width, canvas1.height);

    const ba1 = button.New("Button Text");
    ba1.setBorder(1,1,1,1);
    ba1.setBorderRound(4, 1);
    ba1.setBorderColor(color.Name("black"));
    ba1.setPos(100, 100);
    root1.add(ba1);

    const root2 = new Panel(0, 0);
    root2.setBorder(1,1,1,1);
    root2.setBgColor(color.Name("lightgray"));
    root2.setBorderColor(color.Name("black"));
    root2.setSize(canvas2.width, canvas2.height);

    const b2 = button.New("Button Icon", icon.Check);
    b2.setPos(200, 100);
    root1.add(b2);

    const b3 = button.New("", icon.PlayArrow);
    b3.setPos(340, 100);
    root1.add(b3);

    const b4 = button.New("Text", icon.PlayArrow);
    b4.setPos(420, 100);
    b4.setText("Text2");
    b4.styles().textFont.setSize(20);
    b4.styles().iconFont.setSize(20);
    b4.styles().iconPos = consts.IconBottom;
    b4.styles().pressed.fgColor = color.Name("red");
    b4.subscribe(event.Click, (ev:any) => {
        console.log("b4 on click", ev);
    });
    root1.add(b4);

    const cb1 = checkbox.New("check 1");
    cb1.setPos(10, 10);
    cb1.styles().textFont.setSize(14);
    cb1.styles().iconFont.setSize(14);
    cb1.subscribe(event.Click, (evn:string) => {
        console.log("cb1:", evn, cb1.value());
    });
    cb1.subscribe(event.Change, (evn:string) => {
        console.log("cb1:", evn, cb1.value());
    });
    root1.add(cb1);

    const ra1 = radio.New("radio 1");
    ra1.setPos(200, 10);
    ra1.subscribe(event.Click, (evn: string) => {
        console.log("ra1:", evn, ra1.value());
    });
    ra1.subscribe(event.Change, (evn: string) => {
        console.log("ra1:", evn, ra1.value());
    });
    root1.add(ra1);


    const bb1 = button.New("Button Text", icon.Check);
    bb1.setPos(100, 100);
    bb1.setSize(120, 60);
    root2.add(bb1);

    // Starts rendering
    const rend1 = renderer.New(canvas1, root1);
    rend1.start();
    const rend2 = renderer.New(canvas2, root2);
    rend2.start();

    // Starts event manager
    evm.get().add(canvas1, root1);
    evm.get().add(canvas2, root2);
    evm.get().start();
}
tests.All.set("button", run);
