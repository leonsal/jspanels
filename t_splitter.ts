import * as tests from "./tests";
import * as color from "./core/color";
import {Panel} from "./core/panel";
import * as renderer from "./core/renderer";
import * as evm from "./core/event_manager";
import * as splitter from "./widgets/splitter";

export function run() {

    let canvas = document.createElement("canvas");
    canvas.width = 800;
    canvas.height = 800;
    let body = document.getElementsByTagName("body");
    body[0].appendChild(canvas);

    let ctx = canvas.getContext("2d");
    if (ctx === null) {
        throw new Error("getContext('2d')")
    }

    const root = new Panel(0, 0);
    root.setBgColor(color.Name("blue"));
    root.setBorderColor(color.Name("black"));
    root.setBorder(1, 1, 1, 1);
    root.setBgColor(color.Name("whitesmoke"));
    root.setPadding(10, 10, 10, 10);
    root.setSize(canvas.width, canvas.height);

    const sph = new splitter.Splitter(true);
    sph.setPos(10, 10);
    sph.setSplit(0.25);
    sph.setSize(400, 200);
    sph.setBorder(1,1,1,1);
    sph.setBorderColor(color.Name("black"));
    root.add(sph);

    const spv = new splitter.Splitter(false);
    spv.setPos(10, 300);
    spv.setSplit(0.75);
    spv.setSize(200, 400);
    spv.setBorder(1,1,1,1);
    spv.setBorderColor(color.Name("red"));
    root.add(spv);

    // Starts rendering
    const rend = renderer.New(canvas, root);
    rend.start();

    // Starts event manager
    evm.get().add(canvas, root);
    evm.get().start();
}

tests.All.set("splitter", run);
