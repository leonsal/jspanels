import * as font   from "../core/font";
import * as color  from "../core/color";
import * as rect   from "../core/rect";

export type Style = {
    border:         rect.Border,
    padding:        rect.Border,
    borderColor:    color.Color,
    bgColor:        color.Color,
    fgColor:        color.Color,
    font:           font.Font,
}

//export interface ITarget {
//    setChanged(s: boolean):void;
//}
//
//export class Style2 {
//
//    private _border:         rect.Border;
//    private _padding:        rect.Border;
//    private _borderColor:    color.Color;
//    private _bgColor:        color.Color;
//    private _fgColor:        color.Color;
//    private _font:           font.Font;
//    private _target:         ITarget|null;
//
//    constructor(
//        border:        rect.Border,
//        padding:       rect.Border,
//        borderColor:   color.Color,
//        bgColor:       color.Color,
//        fgColor:       color.Color,
//        font:          font.Font,
//        ) {
//
//        this._border = border;
//        this._padding = padding;
//        this._borderColor = borderColor;
//        this._bgColor = bgColor;
//        this._fgColor = fgColor;
//        this._font = font;
//        this._target = null;
//    }
//
//    setTarget(t: ITarget|null) {
//        this._target = t;
//    }
//
//    get border() {
//        return this._border;
//    }
//
//    set border(b: rect.Border) {
//        this._border = b;
//        this._changed();
//    }
//
//    get padding() {
//        return this._padding;
//    }
//
//    set padding(p: rect.Border) {
//        this._padding = p;
//        this._changed();
//    }
//
//    get borderColor() {
//        return this._borderColor;
//    }
//
//    set borderColor(c: color.Color) {
//        this._borderColor = c;
//        this._changed();
//    }
//
//    get bgColor() {
//        return this._bgColor;
//    }
//
//    set bgColor(c: color.Color) {
//        this._bgColor = c;
//        this._changed();
//    }
//
//    get fgColor() {
//        return this._fgColor;
//    }
//
//    set fgColor(c: color.Color) {
//        this._fgColor = c;
//        this._changed();
//    }
//
//    get font() {
//        return this._font;
//    }
//
//    set font(f: font.Font) {
//        this._font = f;
//        this._changed();
//    }
//
//    _changed() {
//        if (this._target != null) {
//            this._target.setChanged(true);
//        }
//    }
//}