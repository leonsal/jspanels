import * as event from "../core/event";
import * as theme from "../theme/theme";
import {ButtonBase} from "./button";
import * as label from "./label";
import * as button from "./button_styles";

export function New(text: string="") {

    return Object.seal(new Checkbox(text));
}


export class Checkbox extends ButtonBase {

    protected _value:  boolean;

    constructor(text: string="") {

        super(theme.checkbox(), text);
        this.setIcon(this._styles.iconOff);
        this._value = false;
    }

    // Returns the current value of this radio button
    value(): boolean {

        return this._value;
    }

    // Sets the value of this radio button
    setValue(state: boolean) {

        if (this._value === state) {
            return;
        }
        this._value = state;
        if (this._value) {
            this.setIcon(this._styles.iconOn);
        } else {
            this.setIcon(this._styles.iconOff);
        }
        this.setChanged(true);
    }

    // Process events for this radio button.
    // Called by the event manager.
    _onEvent(evn: string, ev:any) {
      
        if (evn === event.MouseUp) {
            if (this._pressed) {
                this.setValue(!this.value());
                this.dispatch(event.Click);
            }
            this._pressed = false;
            this.setChanged(true);
            return true;
        } else {
            return super._onEvent(evn, ev);
        }
    }
}

