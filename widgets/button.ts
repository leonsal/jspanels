import * as panel  from "../core/panel";
import * as label  from "../widgets/label";
import * as event  from "../core/event";
import * as theme  from "../theme/theme";
import * as consts from "./consts";
import * as button from "./button_styles";
import * as util   from "../util/clone";

export function New(text: string, icon="") {

    return Object.seal(new Button(text, icon));
}

export class ButtonBase extends panel.Panel {

    protected _styles:    button.Styles;
    protected _text:      label.Label|null;
    protected _icon:      label.Label|null;
    protected _mouseOver: boolean;
    protected _pressed:   boolean;
    protected _focus:     boolean;

    constructor(styles: button.Styles, text:string|null="", icon:string|null="") {

        super();
        this._styles = styles; 
        this._text = null;          // text label
        this._icon = null;          // icon label
        if (text !== null) {        // if text supplied, creates text label
            this.setText(text);
        }
        if (icon !== null) {        // if icon supplied, creates icon label
            this.setIcon(icon);
        }
        this._mouseOver = false;
        this._pressed = false;
        this._focus = false;
    }

    // Returns the styles of this button.
    styles() : button.Styles {

        return this._styles;
    }

    // Returns the current text of this button
    text() :string {

        if (this._text) {
            return this._text.text();
        }
        return "";
    }

    // Sets this button text creating the text label if necessary
    setText(text: string) {

        if (this._text !== null) {
            this._text.setText(text);
        } else {
            this._text = label.New(text);
            this.add(this._text);
        }
        this.setChanged(true);
    }

    // Sets this button icon creating the icon label if necessary
    setIcon(text: string) {

        if (this._icon !== null) {
            this._icon.setText(text);
        } else {
            this._icon = label.New(text);
            this.add(this._icon);
        }
        this.setChanged(true);
    }

    // Applies the specified style to the base panel and to
    // this button text and/or icon labels.
    _applyStyle(style: button.Style) {

        // Apply style to button base panel
        super.applyStyle(style);

        // Apply style to child labels
        if (this._text) {
            this._text.style().fgColor.setFrom(style.fgColor);
            this._text.style().font.setFrom(this._styles.textFont);
        }
        if (this._icon) {
            this._icon.style().fgColor.setFrom(style.fgColor);
            this._icon.style().font.setFrom(this._styles.iconFont);
        }
    }

    // Updates the button visual style accordingly to its state.
    _updateStyle() {
    
        if (!this.enabled()) {
            this._applyStyle(this._styles.disabled);
            return;
        }
        if (this._focus) {
            this._applyStyle(this._styles.focus);
            return;
        }
        if (this._pressed) {
            this._applyStyle(this._styles.pressed);
            return;
        }
        if (this._mouseOver) {
            this._applyStyle(this._styles.over);
            return;
        }
        this._applyStyle(this._styles.normal);
    }

    render(ctx: CanvasRenderingContext2D) {

        this.update();
        super.render(ctx);
    }

    update() {

        this._updateStyle();
        switch (this._styles.iconPos) {
        case consts.IconLeft:
            this._recalcLeft();
            break;
        case consts.IconTop:
            this._recalcTop();
            break;
        case consts.IconRight:
            this._recalcRight();
            break;
        case consts.IconBottom:
            this._recalcBottom();
            break;
        default:
            throw new Error("Invalid icon position");
        }
    }

    _recalcLeft() {

        // Calculates the button panel minimum content area width and height 
        // to fit exactly the icon/text.
        let mwidth = 0;
        let mheight = 0;
        if (this._icon) {
            this._icon.update();
            mwidth += this._icon.width();
            mheight = this._icon.height();
        }
        if (this._text) {
            this._text.update();
            mwidth += this._text.width();
            if (this._text.height() > mheight) {
                mheight = this._text.height();
            }
        }

        // Sets the button content area
        const cwidth  = Math.max(this.contentWidth(), mwidth);
        const cheight = Math.max(this.contentHeight(), mheight);
        this.setContentSize(cwidth, cheight);

        // Centralize the position of the icon/text
        let posX = (cwidth - mwidth) / 2;
        let posY = (cheight -mheight) / 2;

        // Sets the position of the child labels: icon and text
        if (this._icon) {
            this._icon.setPos(posX, posY);
            posX += Math.round(this._icon.width());
        }
        if (this._text) {
            this._text.setPos(posX, posY);
        }
    }

    _recalcRight() {

        // Calculates the button panel minimum content area width and height 
        // to fit exactly the icon/text.
        let mwidth = 0;
        let mheight = 0;
        if (this._icon) {
            this._icon.update();
            mwidth += this._icon.width();
            mheight = this._icon.height();
        }
        if (this._text) {
            this._text.update();
            mwidth += this._text.width();
            if (this._text.height() > mheight) {
                mheight = this._text.height();
            }
        }

        // Sets the button content area
        const cwidth  = Math.max(this.contentWidth(), mwidth);
        const cheight = Math.max(this.contentHeight(), mheight);
        this.setContentSize(cwidth, cheight);

        // Centralize the position of the icon/text
        let posX = (cwidth - mwidth) / 2;
        let posY = (cheight -mheight) / 2;

        // Sets the position of the child labels: icon and text
        if (this._text) {
            this._text.setPos(posX, posY);
            posX += Math.round(this._text.width());
        }
        if (this._icon) {
            this._icon.setPos(posX, posY);
        }
    }

    _recalcTop() {

        // Calculates the button panel minimum content area width and height 
        // to fit exactly the icon/text.
        let mwidth = 0;
        let mheight = 0;
        if (this._icon) {
            this._icon.update();
            mwidth = this._icon.width();
            mheight += this._icon.height();
        }
        if (this._text) {
            this._text.update();
            if (this._text.width() > mwidth) {
                mwidth = this._text.width();
            }
            mheight += this._text.height();
        }

        // Sets the button content area
        const cwidth  = Math.max(this.contentWidth(), mwidth);
        const cheight = Math.max(this.contentHeight(), mheight);
        this.setContentSize(cwidth, cheight);

        // Centralize the position of the icon/text
        let posY = (cheight -mheight) / 2;

        // Sets the position of the child labels: icon and text
        if (this._icon) {
            let posX = (cwidth - this._icon.width()) / 2;
            this._icon.setPos(posX, posY);
            posY += Math.round(this._icon.width());
        }
        if (this._text) {
            let posX = (cwidth - this._text.width()) / 2;
            this._text.setPos(posX, posY);
        }
    }

    _recalcBottom() {

        // Calculates the button panel minimum content area width and height 
        // to fit exactly the icon/text.
        let mwidth = 0;
        let mheight = 0;
        if (this._text) {
            this._text.update();
            mwidth = this._text.width();
            mheight += this._text.height();
        }
        if (this._icon) {
            this._icon.update();
            if (this._icon.width() > mwidth) {
                mwidth = this._icon.width();
            }
            mheight += this._icon.height();
        }

        // Sets the button content area
        const cwidth  = Math.max(this.contentWidth(), mwidth);
        const cheight = Math.max(this.contentHeight(), mheight);
        this.setContentSize(cwidth, cheight);

        // Centralize the position of the icon/text
        let posY = (cheight -mheight) / 2;

        // Sets the position of the child labels: icon and text
        if (this._text) {
            const posX = (cwidth - this._text.width()) / 2;
            this._text.setPos(posX, posY);
            posY += this._text.height();
        }
        if (this._icon) {
            const posX = (cwidth - this._icon.width()) / 2;
            this._icon.setPos(posX, posY);
        }
    }

    // Process evms for this button or any of its children.
    // Called by the event manager.
    _onEvent(evn: string, ev: any) {

        switch (ev.type) {
        case event.MouseEnter:
            this._mouseOver = true;
            this.setChanged(true);
            return true;
        case event.MouseLeave:
            this._mouseOver = false;
            this._pressed = false;
            this.setChanged(true);
            return true;
        case event.MouseDown:
            this._pressed = true;
            this.setChanged(true);
            return true;
        case event.MouseUp:
            if (this._pressed) {
                this.dispatch(event.Click);
            }
            this._pressed = false;
            this.setChanged(true);
            return true;
        }
        return false;
    }
}


export class Button extends ButtonBase {

    constructor(text="", icon="") {

        super(theme.button(), text, icon);
    }
}


