import * as color from "../core/color";
import * as panel from "./panel_style";

export type Styles = {
    normal:     Style,
    over:       Style,
    focus:      Style,
    disabled:   Style,
}

/** Window style is composed of the base panel style and the Window header style */
export type Style = panel.Style & {
    header: HeaderStyle,
}

/** Window header style */
export type HeaderStyle = panel.Style & {
    fgColor: color.Color;
}