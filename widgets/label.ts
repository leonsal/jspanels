import * as panel from "../core/panel";
import * as theme from "../theme/theme";
import * as util  from "../util/clone";
import * as label from "./label_style";
import * as event from "../core/event_manager";

export function New(text: string) {

    return Object.seal(new Label(text));
}


export class Label extends panel.Panel {

    protected _text:  string;
    protected _style: label.Style;

    constructor(text: string) {

        super();
        this._text = text;
        this._style = theme.label();
    }

    // Returns a reference to this label style
    style() {

        return this._style;
    }

    // Returns the current text of this label
    text() {

        return this._text;
    }

    // Sets the text of this label
    setText(text: string) {

        this._text = text;
        this.setChanged(true);
    }

    setStyle(style: label.Style) {

        this._style = util.clone(style);
    }

    render(ctx: CanvasRenderingContext2D) {

        // Calculates the text width and height, sets this label panel content size
        // and render the base Panel.
        this.update();
        super.render(ctx);

        // Draws the label text over the panel content area
        ctx.save();
        super.clipContent(ctx);
        ctx.textBaseline = "top";
        ctx.font = this._style.font.toString();
        ctx.fillStyle = this._style.fgColor.toString();
        const pos = this.absContentPos();
        ctx.fillText(this._text, pos.x, pos.y);
        ctx.restore();
    }

    // May be called by other widgets which use labels internally.
    update() {

        // Apply the current label style to its base panel
        super.applyStyle(this._style);

        // Calculates label text width and height and sets its panel content size.
        let width = this._style.font.width(this._text);
        this.setContentSize(width, this._style.font.height());
    }
}

