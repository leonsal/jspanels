import * as color  from "../core/color";

export type Styles = {
    normal: Style,
    over:   Style,
    drag:   Style,
}

export type Style = {
    spacerCursorH:      string,         // cursor name for horizontal splitter
    spacerCursorV:      string,         // cursor name for vertical splitter 
    spacerBorderColor:  color.Color,    // spacer border color
    spacerColor:        color.Color,
    spacerSize:         number,
}