import * as color from "../core/color";
import * as font from "../core/font";
import * as panel from "./panel_style";

export type Styles = {
    textFont:   font.Font,
    iconFont:   font.Font,
    iconPos:    string,
    iconOn:     string,         // used by checkbox and radio
    iconOff:    string,         // used by checkbox and radio
    normal:     Style,
    over:       Style,
    focus:      Style,
    pressed:    Style,
    disabled:   Style,
}

export type Style = panel.Style & {
    fgColor: color.Color,
}
