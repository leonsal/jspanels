import * as color  from "../core/color";
import * as rect   from "../core/rect";

export type Style = {
    margin:         rect.Border,
    border:         rect.Border,
    padding:        rect.Border,
    borderRound:    number[]|null,
    borderColor:    color.Color,
    bgColor:        color.Color,
}
