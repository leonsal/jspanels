import * as panel from "../core/panel";
import * as label from "../widgets/label";
import * as theme from "../theme/theme";
import * as tuner from "./tuner_styles";
import * as evm from "../core/event_manager";
import * as event from "../core/event";

export function New(ndigits: number, unit: string) {

    return Object.seal(new Tuner(ndigits, unit));
}

export class Tuner extends panel.Panel {

    protected _styles:    tuner.Styles;
    protected _evm:       evm.EventManager;
    protected _digits:    label.Label[];
    protected _value:     number;
    protected _minValue:  number;
    protected _maxValue:  number;
    protected _index:     number;
    protected _mouseOver: boolean;
    protected _unit:      label.Label;

    constructor(ndigits: number, unit: string) {

        super();
        this._styles = theme.tuner();               // get styles from current theme
        this._evm = evm.get();                    // saved event manager reference
        this._digits = [];                          // list of digits child labels
        this._value = 0;                            // current tuner value
        this._minValue = 0;                         // minimum tuner value
        this._maxValue = Math.pow(10, ndigits)-1;   // maximum tuner value
        this._index = -1;                           // index of current digit (0...ndigits-1)
        this._mouseOver = false;                    // mouse over digit flag

        // Create child labels (digits and separators) 
        for (let i = 0; i < ndigits; i++) {
            // Creates child label for digit
            const l = label.New("0");
            l.style().font = this._styles.textFont;
            this.add(l);
            this._digits.push(l);
            // Subscribe to label events
            l.subscribe(event.MouseEnter, (evn: string, ev: MouseEvent) => this._onMouseEnter(i, ev));
            l.subscribe(event.MouseLeave, (evn: string, ev: MouseEvent) => this._onMouseLeave(i, ev));
            l.subscribe(event.Wheel,      (evn: string, ev: WheelEvent) => this._onWheel(i, ev));
            // Creates child label for group separator if necessary
            const rest = ndigits - 1 - i;
            if (rest > 0 && (rest%3) === 0) {
                const l = label.New(".");
                l.style().font = this._styles.textFont;
                this.add(l);
            }
        }

        // Creates unit label
        this._unit = label.New(unit);
        this._unit.style().font = this._styles.textFont;
        this.add(this._unit);
    }

    get styles() {
        return this._styles;
    }

    // Sets the value of this tuner
    setValue(value: number, sendev=false) {

        if (value < this._minValue || value > this._maxValue) {
            return;
        }

        let val = value;
        const ndigits = this._digits.length;
        for (let i = 0; i < ndigits; i++) {
            const pten = Math.pow(10, ndigits - i - 1);
            const dv = Math.floor(val / pten);
            val = val % pten;
            this._digits[i].setText(dv.toString());
        }
        this._value = value;

        if (sendev) {
            this.dispatch(event.Change, this._value);
        }
        return this;
    }

    // SetValueRange set the range of possible values
    setValueRange(min: number, max: number) {
    
        if (min < 0) {
            min = 0;
        }
        if (min > max) {
            throw new Error("Invalid value range");
        }
        const maxMax = Math.pow(10, this._digits.length-1);
        if (max > maxMax) {
            max = maxMax;
        }
        this._minValue = min;
        this._maxValue = max;
        return this;
    }

    render(ctx: CanvasRenderingContext2D) {

        this._update();
        let totalWidth = 0;
        let height = 0;
        let posX = 0;
        let posY = 0;
        for (const l of this._children) {
            (<label.Label>l).update();
            const width = l.width();
            totalWidth += width;
            if (l.height() > height) {
                height = l.height();
            }
            l.setPos(posX, posY);
            posX += width;
        }
        this.setContentSize(totalWidth, height);
        super.render(ctx);
    }

    // Decrements digit value
    _decDigit() {

        const pten = this._digits.length - this._index - 1;
        const value = this._value - Math.pow(10, pten);
        this.setValue(value, true);
    }

    // Increments digit value
    _incDigit() {
    
        const pten = this._digits.length - this._index - 1;
        const value = this._value + Math.pow(10, pten);
        this.setValue(value, true);
    }

    // Sets key focus to next digit if possible
    _nextDigit() {
    
        if (this._index >= this._digits.length-1) {
            return;
        }
        this._index++;
        this.setChanged(true);
    }
    
    // Sets key focus to previous digit if possible
    _prevDigit() {
    
        if (this._index <= 0) {
            return;
        }
        this._index--;
        this.setChanged(true);
    }

    // Sets the value of the specified digit
    _setDigit(vset: number) {
    
        const digits = [];
        let value = this._value;
        for (let i = 0; i < this._digits.length; i++) {
            const pten = Math.pow(10, this._digits.length - i - 1);
            const dv = Math.floor(value / pten);
            value = value % pten;
            digits.push(dv);
        }
    
        digits[this._index] = vset;
        value = 0;
        for (let i = 0; i < this._digits.length; i++) {
            const pten = Math.pow(10, this._digits.length - i - 1);
            value += digits[i] * pten;
        }
        this.setValue(value, true);
    }

    // Applies the specified style to the base panel and to
    // this tuner child labels.
    _applyStyle(style: tuner.Style) {

        // Apply style to base panel
        super.applyStyle(style);

        // Apply style to all child labels
        let leftZeros = true;
        for (const c of this._children) {
            const cl: label.Label = <label.Label>c;
            // Sets the style for left zeros
            if (leftZeros) {
                if (cl.text() === "0" || cl.text() === ".") {
                    cl.style().fgColor.setFrom(style.digit.leftZeros.fgColor);
                    cl.style().bgColor.setFrom(style.digit.leftZeros.bgColor);
                    continue;
                } else {
                    leftZeros = false;
                }
            }
            cl.style().fgColor.setFrom(style.fgColor);
            cl.style().bgColor.setFrom(style.bgColor);
        }

        // Apply style to current over/focused digit
        if (this._index >= 0) {
            this._digits[this._index].style().bgColor.setFrom(style.digit.bgColor);
            this._digits[this._index].style().fgColor.setFrom(style.digit.fgColor);
        }
    }


    // Process mouse enter event for specified digit
    _onMouseEnter(digit: number, ev: MouseEvent) {

        this._index = digit;
        this._mouseOver = true;
        this._evm.setKeyFocus(this);
        this.setChanged(true);
    }

    // Process mouse leave event for specified digit
    _onMouseLeave(digit: number, ev: MouseEvent) {

        this._mouseOver = false;
        this._index = -1;
        this._evm.setKeyFocus(null);
        this.setChanged(true);
    }

    // Process mouse wheel event for specified digit
    _onWheel(digit: number, ev: WheelEvent) {

        if (ev.deltaY < 0) {
            this._decDigit();
        } else {
            this._incDigit();
        }
    }

    _onKeyDown(ev: KeyboardEvent) {

        if (ev.key >= "0" && ev.key <= "9") {
            const dv = parseInt(ev.key);
            this._setDigit(dv);
            this._nextDigit();
            return;
        }
        switch (ev.key) {
        case "ArrowUp":
            this._incDigit();
            break;
        case "ArrowDown":
            this._decDigit();
            break;
        case "ArrowRight":
            this._nextDigit();
            break;
        case "ArrowLeft":
            this._prevDigit();
            break;
        case "Backspace":
            this._setDigit(0);
            this._prevDigit();
        }
    }

    // Called internally by the event manager with events for this panel.
    // or any of its children.
    // Must return true to stop event propagation or false otherwise.
    _onEvent(evn: string, ev: any) {

        switch(evn) {
        case event.KeyDown:
            this._onKeyDown(<KeyboardEvent>ev);
            return true;
        }
        return false;
    }

    // Updates the visual style accordingly to its state.
    _update() {
    
        if (!this.enabled()) {
            this._applyStyle(this._styles.disabled);
            return;
        }
        if (this._mouseOver) {
            this._applyStyle(this._styles.over);
            return;
        }
        this._applyStyle(this._styles.normal);
    }
}

