import * as panel from "../core/panel";
import * as evm from "../core/event_manager";
import * as event from "../core/event";
import * as theme from "../theme/theme";
import * as window from "./window_styles";


export class Window extends panel.Panel {

    protected _styles: window.Styles;      // current styles
    protected _mouseOver: boolean;          
    protected _focus: boolean;
    protected _header: Header;             // header panel
    protected _client: panel.Panel;        // client area panel

    constructor(width: number, height: number) {

        super();
        this.setSize(width, height);
        this._styles = theme.window();
        this._mouseOver = false;
        this._focus = false;
        this._header = new Header();
        this._client = new panel.Panel(0, 0);

        this.add(this._header);
        this.add(this._client);
    }

    render(ctx: CanvasRenderingContext2D) {

        this.update();
        super.render(ctx);
    }

    update() {

        this._updateStyle();
        // Calculates positions and sizes of internal panels


    }

    protected _updateStyle() {

        if (!this.enabled()) {
            this._applyStyle(this._styles.disabled);
            return
        }
        if (this._mouseOver) {
            this._applyStyle(this._styles.over);
            return
        }
        if (this._focus) {
            this._applyStyle(this._styles.focus);
            return
        }
        this._applyStyle(this._styles.normal);
    }

    protected _applyStyle(style: window.Style) {

        // Apply style to base panel
        super.applyStyle(style);
        // Apply style to internal header panel
        this._header._applyStyle(style.header);
    }
}

class Header extends panel.Panel {

    constructor() {
        super();
    }

    _applyStyle(style: window.HeaderStyle) {

        // Apply style to base panel
        super.applyStyle(style);

    }
}