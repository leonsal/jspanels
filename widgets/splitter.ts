import * as panel from "../core/panel";
import * as evm from "../core/event_manager";
import * as event from "../core/event";
import * as theme from "../theme/theme";
import * as splitter from "./splitter_styles";
import * as color from "../core/color";

export class Splitter extends panel.Panel {

    protected _p0: panel.Panel              // left/top panel
    protected _p1: panel.Panel              // right/bottom panel
    protected _spacer: panel.Panel          // spacer panel
    protected _styles: splitter.Styles;     // current splitter styles
    protected _horiz: boolean;              // splitter orientation 
    protected _pos: number;                 // current spacer relative position
    protected _posStart: number;            // start of drag spacer relative position
    protected _pressed: boolean;            // mouse button pressed flag
    protected _mouseOver: boolean;          // mouse over spacer flag
    protected _cursor: string;              // name of css cursor to set 

    constructor(horizontal: boolean) {

        super();
        this._horiz = horizontal;
        this._styles = theme.splitter();
        this._p0 = new panel.Panel(0, 0);
        this._p1 = new panel.Panel(0, 0);
        this._spacer = new panel.Panel(0, 0);
        this._pos = 0.5;
        this._posStart = 0;
        this._pressed = false;
        this._mouseOver = false;
        this._cursor = "";

        this.add(this._p0);
        this.add(this._p1);
        this.add(this._spacer);

        if (this._horiz) {
            this._spacer.setBorder(0, 1, 0, 1);
        } else {
            this._spacer.setBorder(1, 0, 1, 0);
        }

        // Subscribe to spacer mouse events
        this._spacer.subscribeThis(event.MouseDown, this, this._onSpacerMouse);
        this._spacer.subscribeThis(event.MouseUp, this, this._onSpacerMouse);
        this._spacer.subscribeThis(event.MouseMove, this, this._onSpacerMouse);
        this._spacer.subscribeThis(event.MouseEnter, this, this._onSpacerMouse);
        this._spacer.subscribeThis(event.MouseLeave, this, this._onSpacerMouse);

        this.update();
    }

    /**
     *  Sets the relative position of the spacer
     * @pos relative spacer position from 0 to 1 
     */
    setSplit(pos: number) {

        if (pos === this._pos) {
            return;
        }

        if (pos < 0) {
            this._pos = 0;
        } else if (pos > 1) {
            this._pos = 1;
        } else {
            this._pos = pos;
        }
        this.setChanged(true);
    }

    /**
     * Returns the current relative position of the spacer
     * @return relative spacer position from 0 to 1
     */
    split() :number {

        return this._pos;
    }

    render(ctx: CanvasRenderingContext2D) {

        this.update();
        // Sets the canvas cursor if required
        if (this._cursor) {
            ctx.canvas.style.cursor = this._cursor;
            this._cursor = "";
        }
        super.render(ctx);
    }

    /**
     * Applies the current style and recalculates the position and sizes of internal panels.
     */
    update() {

        this._updateStyle();
        const width = this.contentWidth();
        const height = this.contentHeight();
        if (this._horiz) {
            // Calculate x position for spacer panel
            let spx = width * this._pos - this._spacer.width() / 2;
            if (spx < 0) {
                spx = 0;
            } else if (spx > width - this._spacer.width()) {
                spx = width - this._spacer.width();
            }
            // Left panel
            this._p0.setPos(0, 0);
            this._p0.setSize(spx, height);
            // Spacer panel
            this._spacer.setPos(spx, 0);
            this._spacer.setHeight(height);
            // Right panel
            this._p1.setPos(spx + this._spacer.width(), 0);
            this._p1.setSize(width - spx - this._spacer.width(), height);
        } else {
            // Calculate y position for spacer panel
            let spy = height * this._pos - this._spacer.height() / 2;
            if (spy < 0) {
                spy = 0;
            } else if (spy > height - this._spacer.height()) {
                spy = height - this._spacer.height();
            }
            // Top panel
            this._p0.setPos(0, 0);
            this._p0.setSize(width, spy);
            // Spacer panel
            this._spacer.setPos(0, spy);
            this._spacer.setWidth(width);
            // Bottom panel
            this._p1.setPos(0, spy + this._spacer.height());
            this._p1.setSize(width, height - spy - this._spacer.height());
        }
    }

    protected _applyStyle(style: splitter.Style) {

        this._spacer.setBorderColor(style.spacerBorderColor);
        this._spacer.setBgColor(style.spacerColor);
        if (this._horiz) {
            this._spacer.setWidth(style.spacerSize);
        } else {
            this._spacer.setHeight(style.spacerSize);
        }
    }

    protected _setCursor() {

        if (this._pressed) {
            if (this._horiz) {
                this._cursor = this._styles.drag.spacerCursorH;
            } else {
                this._cursor = this._styles.drag.spacerCursorV;
            }
            return;
        }
        if (this._mouseOver) {
            if (this._horiz) {
                this._cursor = this._styles.over.spacerCursorH;
            } else {
                this._cursor = this._styles.over.spacerCursorV;
            }
            return;
        }
        this._cursor = "default";
    }

    protected _updateStyle() {

        if (this._pressed) {
            this._applyStyle(this._styles.drag);
            return
        }
        if (this._mouseOver) {
            this._applyStyle(this._styles.over);
            return
        }
        this._applyStyle(this._styles.normal);
    }

    /** Process events for the internal spacer panel */
    protected _onSpacerMouse(evn: string, ev: event.MouseEventCanvas) {

        switch (evn) {
            case event.MouseEnter:
                this._mouseOver = true;
                this._setCursor();
                this.setChanged(true);
                break;
            case event.MouseLeave:
                this._mouseOver = false;
                this._setCursor();
                this.setChanged(true);
                break;
            case event.MouseDown:
                this._pressed = true;
                if (this._horiz) {
                    this._posStart = ev.canvasX;
                } else {
                    this._posStart = ev.canvasY;
                }
                this._setCursor();
                this.setChanged(true);
                evm.get().setMouseFocus(this._spacer);
                break;
            case event.MouseUp:
                this._pressed = false;
                this._setCursor();
                this.setChanged(true);
                evm.get().setMouseFocus(null);
                break;
            case event.MouseMove:
                if (!this._pressed) {
                    break;
                }
                let delta = 0;
                let pos = this._pos;
                if (this._horiz) {
                    delta = ev.canvasX - this._posStart;
                    this._posStart = ev.canvasX;
                    pos += delta / this.contentWidth();
                } else {
                    delta = ev.canvasY - this._posStart;
                    this._posStart = ev.canvasY;
                    pos += delta / this.contentHeight();
                }
                this.setSplit(pos);
        }
    }

    /** Process events for the main splitter panel */
    _onEvent(evn: string, ev: any): boolean {

        switch (evn) {
            case event.Resize:
                break;
        }

        return false;
    }
}
