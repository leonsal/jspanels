import * as font   from "../core/font";
import * as color  from "../core/color";
import * as rect   from "../core/rect";
import * as panel from "./panel_style";

export type Styles = {
    textFont:   font.Font,
    normal:     Style,
    over:       Style,
    focus:      Style,
    disabled:   Style,
}

export type Style = panel.Style & {
    fgColor:        color.Color,
    digit:  {
        leftZeros: {
            bgColor: color.Color,
            fgColor: color.Color,
        }
        bgColor:    color.Color,
        fgColor:    color.Color,
    }
}
