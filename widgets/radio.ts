import * as event from "../core/event";
import * as evm from "../core/event_manager";
import * as theme from "../theme/theme";
import * as button from "./button_styles";
import {ButtonBase, Button} from "./button";


export function New(text: string, group: string="") {

    return Object.seal(new Radio(text, group));
}


export class Radio extends ButtonBase {

    protected _value:  boolean;
    protected _group:  string;

    constructor(text: string, group: string="") {

        super(theme.radio(), text);
        this._value = false;
        this._group = group;
        this.setIcon(this._styles.iconOff);
        this.setGroup(group);
    }

    // Returns the value of this radio button
    value(): boolean {

        return this._value;
    }

    // Sets the value of this radio button
    setValue(value: boolean) {

        if (this._value === value) {
            return;
        }
        this._value = value;
        if (this._value) {
            this.setIcon(this._styles.iconOn);
        } else {
            this.setIcon(this._styles.iconOff);
        }
        this.setChanged(true);
        if (value && this._group !== "") {
            evm.get().dispatch(event.Group, this);
        }
        this.dispatch(event.Change);
    }

    // Returns the current group of this radio button.
    // Returns empty string for no group.
    group() {

        return this._group;
    }

    // Sets the group for this radio button.
    // Empty string removes it from the previous group
    setGroup(group: string) {

        this._group = group;
        evm.get().unsubscribeThis(event.Group, this, this._onGroup);
        if (this._group != "") {
            evm.get().subscribeThis(event.Group, this, this._onGroup);
        }
    }

    // Called when any radio button is clicked 
    _onGroup(evn:string, r: Radio) {

        // If the clicked radio button is this, nothing to do.
        if (r === this) {
            return;
        }
        if (r.group() == this._group) {
            this.setValue(false);
        }
    }

    // Process events for this checkbox.
    // Called by the event manager.
    _onEvent(evn: string, ev:any) {
      
        if (evn === event.MouseUp) {
            if (this._pressed) {
                this.setValue(true);
                this.dispatch(event.Click);
            }
            this._pressed = false;
            this.setChanged(true);
            return true;
        } else {
            return super._onEvent(evn, ev);
        }
    }
}
